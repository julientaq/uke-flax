// lets you manualy add classes to some pages elements 
// to simulate page floats.
// works only for elements that are not across two pages


let classElemFloatNextTop = "page-float-next-top"; // ← class of floated elements on next page
let classElemFloatNextBottom = "page-float-next-bottom"; // ← class of floated elements bottom on next page



class floatNext extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatNextTop = [];
    this.floatNextBottom = [];
    this.token;
  }
  beforeParsed(content) {
  }

  layoutNode(node) {
    // If you find a float page element, move it in the array,
    if (node.nodeType == 1 && node.classList.contains(classElemFloatNextTop)) {
      let clone = node.cloneNode(true);
      this.floatNextTop.push(clone);
      // Remove the element from the flow by hiding it.
  
      node.style.display = 'none';
    }
    if (node.nodeType == 1 && node.classList.contains(classElemFloatNextBottom)) {
      let clone = node.cloneNode(true);
      this.floatNextBottom.push(clone);
      // Remove the element from the flow by hiding it.
  
    }
  }

  beforePageLayout(page, content, breakToken) {
    //console.log(breakToken);
    // If there is an element in the floatPageEls array,
    if (this.floatNextTop.length >= 1) {
      // Put the first element on the page.
      page.element.querySelector(".pagedjs_page_content").insertAdjacentElement('afterbegin', this.floatNextTop[0]);
      this.floatNextTop.shift();
    }
    if (this.floatNextBottom.length >= 1) {
      // Put the first element on the page.
      page.element.querySelector(".pagedjs_page_content").insertAdjacentElement('afterbegin', this.floatNextBottom[0]);
      this.floatNextBottom.shift();
    }
  }

  // works only with non breaked elements
  afterPageLayout(page, content, breakToken) {
 
    // try fixed bottom if requested
    if (page.querySelector("." + classElemFloatNextBottom)) {
      var bloc = page.querySelector("." + classElemFloatNextBottom);
      bloc.classList.add("absolute-bottom");
    }
  }

}
Paged.registerHandlers(floatNext);



async function awaitImageLoaded(image) {
  return new Promise((resolve) => {
    if (image.complete !== true) {
      image.onload = function () {
        let { width, height } = window.getComputedStyle(image);
        resolve(width, height);
      };
      image.onerror = function (e) {
        let { width, height } = window.getComputedStyle(image);
        resolve(width, height, e);
      };
    } else {
      let { width, height } = window.getComputedStyle(image);
      resolve(width, height);
    }
  });
}
