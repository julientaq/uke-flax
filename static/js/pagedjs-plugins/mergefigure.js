class figureMerge extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  afterParsed(content) {
    let figures = content.querySelectorAll("figure");
    figures.forEach((fig) => {
        // console.log(fig.previousElementSibling.tagName);
      if (
        fig.previousElementSibling &&
        fig.previousElementSibling.tagName == "FIGURE"
      ) {
        fig.previousElementSibling.insertAdjacentHTML(
          "beforeend",
          fig.innerHTML
        );
        fig.remove();
      }
    });
  }
}

Paged.registerHandlers(figureMerge);
