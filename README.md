BUILD

→ install eleventy

```sh
npm install -g @11ty/eleventy
```
→ or run eleventy 

```sh
npx @11ty/eleventy --serve
```


## Using nunjucks for templating


IDEAS:

- set/get reading position with cookies? (should we?) or use the browser save.
“last time you read this book, you were at this position. Do you want to resume your reading?” → netflix reading :D