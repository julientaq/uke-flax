---
title: the book of manu
authors: manu
book: chaussette
layout: "bookindex.njk"
menu: false
pdfname: uke.pdf
epubname: uke.epub
order: 0
tags: 
  - book
---


### Welcome to the many book book. You can download the <strong ><a style="font-weight: 800" href='{{"/" | url}}output/uke.pdf'>pdf</a></strong> or the **<a style="font-weight: 800" href='{{"/" | url}}output/uke.epub'>epub</a>**, or read online. 


