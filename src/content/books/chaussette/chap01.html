---
order: 1
title: 'The rise of Journal Development'
menu: false
book: chaussette
layout: bookchapter.njk
---

<h2 class="c9" id="h.5x9q6ho5ie9l">
  <span class="c10">The development of a researcher-focused structure</span>
</h2>
<p class="c0">
  <span class="c1"
    >The brainchild for Journal Development (JDev) came from years of experience
    working within traditional publishing houses and identifying bottlenecks
    that affect and restrict the publishing life cycle and the value being
    created for researchers.
  </span>
</p>
<p class="c0">
  <strong
    >Typically, publishing departments have been structured in silos </strong
  >(marketing, editorial, sales, operations, and data) that work independently
  of each other towards similar but unshared goals.
</p>
<p class="c0">
  <strong
    >When the primary customer was the librarian, the separation between these
    teams was more understandable:</strong
  >&nbsp;editorial teams worked with researcher groups to develop the journal’s
  content; the operations teams ensured the content was published correctly; the
  marketing teams promoted published content; and the sales teams sold
  subscriptions to librarians.
</p>
<figure>
  <img
    alt=""
    src="/images/1RiseofJournalDevelopment/image1.jpg"
    style=""
    title=""
  />
  <figcaption>
    <i
      >The traditional structure of teams in publishing — little overlap with
      ownership of sections of the workflow.</i
    >
  </figcaption>
</figure>
<p class="c0">
  This approach led teams to focus on their own value stream at the expense of
  researcher (customer) needs, often causing breakdowns in communications
  between departments, delays in output, and the overall goal being lost in
  translation.
</p>
<p class="c0">
  However,
  <strong
    >over time, the requirements of a publishing department have shifted </strong
  >for many reasons, including the move to digital with much less reliance on
  print; the rise in additional publishing models including open access; and the
  growth and size of the market.
</p>
<p class="c0">
  <strong
    >JDev has therefore developed in response to market forces and a need to
    create a multifunctional department that comprises expertise across the
    scope of researcher needs</strong
  >. Our team draws together skills in marketing, editorial, customer knowledge,
  data, operations, and analytics — with the ultimate goal to grow and develop
  our journals.
</p>
<p class="c0">
  To achieve this, JDev uses a blended team-of-teams structure that empowers
  team members to drive development with company-wide impact and encourages
  <strong
    >a high-performing and inclusive culture where everyone gets
    involved</strong
  >.
</p>
<p class="c0">
  Each team is dedicated to a specific customer group (author, editor, reviewer,
  or reader) or a specific aim (content development, regional development, open
  science) — all supported by the same data and infrastructure. By blending
  people of different backgrounds and aligning objectives within a team with a
  singular focus, we <strong>enhance our capability and productivity</strong>,
  <strong
    >reduce the risk of misaligned objectives, and increase value for
    all</strong
  >.
</p>
<figure>
  <img
    alt=""
    src="/images/1RiseofJournalDevelopment/image5.jpg"
    style=""
    title=""
  />
  <figcaption>
    <i><strong>The structure</strong></i
    ><i
      >&nbsp;under Journal Development — blended teams, close collaboration,
      increased value.</i
    >
  </figcaption>
</figure>
<h2 class="c9" id="h.hsdyvkaq3qna">
  What are the benefits of this style of working?
</h2>
<p class="c8">
  A faster, more effective way of working, with greater scalability, seamless
  collaboration, clarity of purpose, improved morale, and a greater level of
  understanding across the board — all of which results in
  <strong>a better service being provided to our </strong
  ><strong>researcher</strong><strong>&nbsp;customers.</strong>&nbsp;
</p>
<p class="c8">
  This streamlined, highly collaborative, customer-focused way of working
  reflects our overall mission:
</p>
<p class="c6 quote">
  “...to advance scientific research by reducing friction between researchers,
  research outputs, platforms, services, and organizations. To succeed in this
  mission, we must work with other like-minded individuals and organizations to
  overcome the legal, technical, and cultural barriers that limit the
  dissemination and reuse of scholarly research.”
</p>
<h2 class="c9" id="h.pmumd5oklnqe">How did we get where we are today?</h2>
<p class="c0">
  We have&nbsp;a long and rich history, all of which has contributed to our
  journey towards the JDev structure we have today.
  <strong
    >Our agility as a business has enabled us to be responsive to the
    market</strong
  >&nbsp;and evolve to provide innovative approaches to serving our research
  communities.
</p>
<figure>
  <img
    alt=""
    src="/images/1RiseofJournalDevelopment/image2.jpg"
    style=""
    title=""
  />
  <figcaption><i>A short history of the rise of JDev</i></figcaption>
</figure>
<h2 class="c9" id="h.i4g9fyepe9ve">How does JDev build strategy?</h2>
<p class="c0">
  We have a straightforward approach to achieving our overall mission:
  <strong>we have six strategic pillars that do not change</strong>. Everything
  we do aims to achieve at least one of these pillars, and each of these pillars
  drives our overall mission.
</p>
<figure>
  <img
    alt=""
    src="/images/1RiseofJournalDevelopment/image6.jpg"
    style=""
    title=""
  />
  <figcaption>
    <i
      >Our six strategic pillars — how we aim to become the leading open access
      publishing services provider built on a foundation of our teams, systems,
      and data.</i
    >
  </figcaption>
</figure>
<p class="c0">
  Within each pillar we have identified key aims that will help us to ensure
  success; these are subject to review every three years but will continue until
  they are achieved.
</p>
<figure>
  <img
    alt=""
    src="/images/1RiseofJournalDevelopment/image3.jpg"
    style=""
    title=""
  />
  <figcaption>
    <i><strong>Key aims to achieve </strong></i><i><strong>JDev </strong></i
    ><i><strong>strategy pillars</strong></i>
  </figcaption>
</figure>
<h2 class="c9" id="h.3jpcykwczxjm">
  How does JDev fit within the teams that manage publishing?
</h2>
<p class="c0">
  Multiple teams support the publishing process of our journals.
  <strong>JDev oversees everything at the journal level</strong>&nbsp;—
  researcher engagement (editor recruitment and training, reviewer reward,
  author attainment, reader engagement), submission driving, journal websites,
  indexing, and dissemination. The other teams each support different functions
  of the publishing process. Teams are spread through different departments of
  Wiley but work together as a whole to support the publication, management, and
  development of our journal portfolio.
</p>
<figure>
  <img
    alt=""
    src="/images/1RiseofJournalDevelopment/image7.jpg"
    style=""
    title=""
  />
  <figcaption>
    <i><strong>The</strong></i
    ><i
      >&nbsp;teams that work together to support and manage our publishing
      portfolio</i
    >
  </figcaption>
</figure>
<figure>
  <img
    alt=""
    src="/images/1RiseofJournalDevelopment/image4.jpg"
    style=""
    title=""
  />
</figure>
<h6 class="c7" id="h.bfjwcl2cqyj2">
  <i>JDev organization chart, demonstrating our team-of-teams structure.</i>
</h6>
<h3 class="c16" id="h.34p9wsuikhsr">
  What are our main challenges and how do we overcome them?
</h3>
<p class="c0">
  A potential risk of our blended structure is the blurring of boundaries
  between roles. To ensure this does not create issues within the team,
  <strong
    >we have developed a culture of getting involved and hiring teammates who
    are open to being transparent and sharing tasks.</strong
  >&nbsp;We ensure that colleagues have specific areas of responsibility and
  know who to contact or loop in when interacting in a particular area of the
  business. This focus on openness, curiosity, and collaboration leads to
  greater levels of personal development and varied work for everyone involved
  in JDev.
</p>
<p class="c0">
  Another challenge is the absence of an internal editorial team to provide
  topic-specific support.
  <strong
    >We have adapted and implemented processes that bypass reliance on an
    internal publishing team. </strong
  >For instance, Marketing Managers have guidelines on checking content to
  promote in campaigns. At the same time, our Data &amp; Operations team uses
  their technical expertise to create programs that guarantee a high level of
  accuracy when selecting articles for promotion.
</p>
<p class="c0">
  Our automated and at-scale approach to researcher communications and
  engagement enables us to rely on external editors for full editorial support
  without the need for an internal editor role. Our data provides a full picture
  of researcher performance and behavior, which we can report on and use to
  inform editor and reviewer training and development.
</p>
<p class="c0">
  We need to make sure we continue to evolve in the ever-changing world of
  scholarly communication and use our data-, infrastructure- and teams-focused
  approach to best serve the research community
</p>
<h3 class="c16" id="h.de7x35247kgo">What is the future of JDev?</h3>
<p class="c0">
  Our innovative approach to breaking down the barriers between the teams
  involved in the development of journals, as well as continuing to enhance our
  understanding of researchers, provides us with many opportunities to grow.
</p>
<p class="c0">
  Key ways in which can continue to evolve and lead the publishing industry
  include:
</p>
<ul class="c19 lst-kix_d4mi6gioc36j-0 start">
  <li class="c0 c5 li-bullet-0">
    developing a wider-ranging ability to understand our different researcher
    customers and personalize engagement with them at scale
  </li>
  <li class="c0 c5 li-bullet-0">
    continually find new ways to leverage the understanding we have about our
    researchers and the publishing process
  </li>
  <li class="c0 c5 li-bullet-0">
    defining the ways Open Science can be developed to serve the global
    researcher community effectively and efficiently
  </li>
  <li class="c0 c5 li-bullet-0">
    leveraging our understanding of researchers and our leading infrastructure
    to provide the best value for all
  </li>
</ul>
