---
order: 4
title: 'Data and systems'
menu: false
book: chaussette
layout: bookchapter.njk
---

<h2 class="c19" id="h.e3q751vi4rqo">
  <span class="c20 c25">A brand built on innovation </span>
</h2>
<p class="c2">
  <span>Ahmed Hindawi, our founder, set out with </span
  ><strong>a desire to do things differently from other publishers</strong>. The
  company understood the difficulties that publishers face when
  working&nbsp;across multiple systems. As a result, they began working on a
  system that would offer
  <strong>a full suite of end-to-end publishing services on one platform</strong
  >, specifically designed for the open access publishing model. This process
  gave the company experience in software development and
  <strong>ingrained a culture of innovation and agility </strong>that still
  thrives today. The eventual output of this end-to-end project was a platform
  called <strong>Phenom</strong>.
</p>
<h3 class="c1" id="h.aen9ftjlvm6b">The arrival of Phenom</h3>
<p class="c2">
  After a great deal of development, the first iteration of Phenom was released
  as
  <strong
    >an open source, end-to-end platform that allowed publishers to facilitate
    the entire submission-to-peer-review-to-publication process on one
    platform</strong
  >.
</p>
<p class="c2">
  Its components were modulated, allowing publishers to customize the extent to
  which Phenom handled the end-to-end process. Since the first iteration, this
  modulation has also been of benefit to developers, as component parts can be
  developed independently of one another — a process that continues today.
</p>
<figure>
  <img alt="" src="/images/4DataSystems/image1.jpg" style="" title="" />
  <figcaption>
    <i
      >An example workflow of the end-to-end services Phenom offers open access
      publishers.</i
    >
  </figcaption>
</figure>
<p class="c2">
  As we migrated our journals over to Phenom, another striking benefit of the
  system became clear: <strong>consolidated data</strong>. All actions on Phenom
  create ‘events’ that can be reported in one place. This centralization of
  end-to-end publishing data is a key enabler of many of the data systems that
  have allowed Journal Development to thrive.
</p>
<h2 class="c19" id="h.ypwnzurudncu">Data-driven Journal Development</h2>
<p class="c2">
  The availability of Phenom data was a significant driving factor in the
  development of data systems for Journal Development (JDev).
  <strong
    >Teams had access to real-time information on author, editor, and reviewer
    activity</strong
  >&nbsp;— providing the potential for much more relevant and timely
  communications with researchers, something the industry has traditionally
  struggled with.
</p>
<p class="c2">
  However, the data needed to be professionally cleaned, handled, and processed
  for a marketing use case. Although initial efforts were made to do this
  through off-the-shelf offerings, it quickly became apparent that
  <strong
    >Journal Development would need dedicated data and marketing technology
    support to gain the most insight into, and fully utilize, the data they had.
  </strong>
</p>
<h2 class="c19" id="h.g67ipkuuzjvz">
  <strong>Journal Development</strong><strong>&nbsp;Data &amp; Ops team</strong>
</h2>
<p class="c2">
  <br />Following the demand for a dedicated technical resource, the Journal
  Development Data and Operations (Data &amp; Ops) team was formed to support
  internal stakeholders.
</p>
<h3 class="c1" id="h.mnrqurdh3gg5">Operational ethos</h3>
<p class="c2">
  From the beginning, the team decided that operationally it was important to
  establish a foundational ethos on which systems could be developed. This can
  be broken into the following points:
</p>
<ul class="c9 lst-kix_h21ixfag28kh-0 start">
  <li class="c2 c5 li-bullet-0">
    <strong>Excellence at delivery</strong><br />Ensure all platforms and
    campaigns make the best use of the technology available and meet the
    objectives of stakeholders
  </li>
  <li class="c2 c5 li-bullet-0">
    <strong>Responsible and relevant use of user data</strong><br />Protect and
    enhance user data where possible to create the best solutions for use cases,
    while still adhering to user permissions
  </li>
  <li class="c2 c5 li-bullet-0"><strong>Development agility</strong></li>
</ul>
<p class="c2 c27">
  Create and maintain a&nbsp;dedicated development environment, allowing fast
  prototyping and innovation where required
</p>
<h3 class="c1" id="h.w7dupvebvtoq">Long-term aims</h3>
<ul class="c9 lst-kix_rk0xttm7bt6g-0 start">
  <li class="c2 c5 li-bullet-0">
    <strong>Be the best Data and </strong><strong>Marketing Tech</strong
    ><strong>nology resource in the industry</strong>
  </li>
</ul>
<p class="c2 c27">
  Be more agile, smarter, and compliant than any other operationally focused
  data team amongst the brand’s competitors
</p>
<ul class="c9 lst-kix_a1ldz8eh4ry9-0 start">
  <li class="c2 c5 li-bullet-0"><strong>Empower the researcher</strong></li>
</ul>
<p class="c2 c27">
  Using data from our analysis, cycle insight back to researchers allowing them
  to make better decisions about their publishing choices and further the
  mission of open science
</p>
<ul class="c9 lst-kix_5489zkqjjk7-0 start">
  <li class="c2 c5 li-bullet-0">
    <strong>Develop team members</strong><br />Support the team to become future
    leaders in the space, advance their careers and advocate for the industry,
    encouraging other technical specialists to work in the space
  </li>
</ul>
<p class="c2">
  The resulting team covers three core functions: Data Engineering, Reporting
  and Insight, and Marketing Operations.
</p>
<figure>
  <img alt="" src="/images/4DataSystems/image3.jpg" style="" title="" />
</figure>
<h6 class="c10" id="h.8w6tit45miar"><i>Teams within Data &amp; Ops</i></h6>
<p class="c2">
  The team’s key stakeholder relationships are with the other specialist units
  within Journal Development. Typically, the stakeholder relationships involve
  consultancy, requests, and delivery of campaigns against a submitted brief.
</p>
<figure>
  <img alt="" src="/images/4DataSystems/image9.jpg" style="" title="" />
  <figcaption><i>Teams that Data &amp; Ops support</i></figcaption>
</figure>
<h2 class="c19" id="h.w0aadfckcmos">Top-level approaches and systems</h2>
<p class="c2">
  The team has established a variety of approaches and top-level system
  architectures that underpin day-to-day activity. The section below outlines
  these approaches and systems but remains neutral to the specific technology
  deployed at the time of writing. This is deliberate as we want to maintain
  <strong
    >a strong emphasis on portability and reduce dependency on any specific
    service platform</strong
  >.
</p>
<h3 class="c1" id="h.47uvb47ibob1">What is our approach to data collection?</h3>
<p class="c2">
  Data collection happens constantly across both first- and third-party
  platforms. Regardless of what data is being collected, these three basic
  principles apply:
</p>
<ol class="c9 lst-kix_2qclrfffcksx-0 start" start="1">
  <li class="c2 c5 li-bullet-0" data-li="1">
    Collect&nbsp;what is <strong>possible</strong>
  </li>
  <li class="c2 c5 li-bullet-0" data-li="2">
    Process&nbsp;what is <strong>relevant</strong>
  </li>
  <li class="c2 c5 li-bullet-0" data-li="3">
    Action what is <strong>required and permissioned</strong>
  </li>
</ol>
<p class="c2 c11"><strong></strong></p>
<p class="c2 c11"><strong></strong></p>
<p class="c2 c38 c11"><strong></strong></p>
<figure>
  <img alt="" src="/images/4DataSystems/image4.jpg" style="" title="" />
  <figcaption><i>Example user data approach</i></figcaption>
</figure>
<h4 class="c21 c22" id="h.jjc0s9pnow02">Core data objects collected</h4>
<p class="c2">
  The objects below represent the main data objects the Journal Development Data
  &amp; Ops team collect, process, and action to meet Journal Development
  requirements. There are other use cases where objects not on this list may
  also be ingested and processed for specific functions.
</p>
<h4 class="c31 c21" id="h.95b17mtpwmcx">Researcher [user] data</h4>
<ul class="c9 lst-kix_e88cgk49i8y9-0 start">
  <li class="c2 c5 li-bullet-0">
    Collected through Phenom or through partnerships, where permissioned
  </li>
  <li class="c2 c5 li-bullet-0">
    Enhanced in-house with data from Web Analytics and third-party vendors
  </li>
</ul>
<h4 class="c21 c31" id="h.qzt0ax9p3a43">Manuscript and journal data</h4>
<ul class="c9 lst-kix_asvjrk4xhuq8-0 start">
  <li class="c2 c5 li-bullet-0">Collected through Phenom</li>
  <li class="c2 c5 li-bullet-0">
    Enhanced in-house with data from third-party vendors
  </li>
</ul>
<h4 class="c31 c21" id="h.rm1xpcmha1ck">Event and behavioral data</h4>
<ul class="c9 lst-kix_679k96r98xwc-0 start">
  <li class="c2 c5 li-bullet-0">Collected through Web Analytics and Phenom</li>
  <li class="c2 c5 li-bullet-0">
    Enhanced in-house with data from third-party vendors
  </li>
</ul>
<h3 class="c1" id="h.xd8nsodu09x3">What is our approach to augmentation?</h3>
<p class="c2">
  Augmentation of data refers to
  <strong>the improvement of the level of data surrounding a data object</strong
  >. For example, a basic user profile might include a name and an email
  address. An augmented user profile might include a name, email, and career
  history. <br />
</p>
<p class="c2">
  Where opportunities to augment data&nbsp;are present and are relevant to a use
  case, the typical approach undertaken is to try and use industry-recognized
  identifiers as key connectors between datasets. Where these are not available,
  we look to create alternative linking methods to create commonalities between
  datasets. This can involve amending data collection methods on platforms to
  capture a metric or dimension that is known to exist in other datasets.
  <br />&nbsp;
</p>
<figure>
  <img alt="" src="/images/4DataSystems/image2.jpg" style="" title="" />
  <figcaption>
    <i>Example augmentation of a user profile using ORCID</i>
  </figcaption>
</figure>
<h3 class="c1" id="h.atjcgdlnmfti">
  What is our approach to storage and syndication?
</h3>
<p class="c2">
  Storage and syndication are key considerations to how Data &amp; Ops develop
  and distribute insights and audiences for stakeholders. Data Syndication is
  the process of sending data to other systems to deliver the same message
  across multiple systems. For example:
</p>
<ol class="c9 lst-kix_lme913t0zdij-0 start" start="1">
  <li class="c2 c5 li-bullet-0" data-li="1">
    An audience list is created in the database.
  </li>
  <li class="c2 c5 li-bullet-0" data-li="2">
    It is then syndicated to an email system and social media for use in a
    campaign.
  </li>
</ol>
<p class="c2">
  After a period of experimentation, it was established that the best option for
  the department would be to <strong>develop our own, </strong
  ><strong>customized</strong><strong>&nbsp;database</strong>. This would allow
  for maximum flexibility to create data assets that could meet marketing use
  cases and could operate outside of the development lifecycle of other systems.
</p>
<h4 class="c22 c21" id="h.2ii5ezxm9y5z">
  <strong>Storage and syndication through the </strong>Marketing Intelligence
  Database (MIDB)
</h4>
<p class="c2">
  The MIDB was created to be a central hub for Journal Development insight,
  while also allowing for a centralized&nbsp;place to store, process, and
  segment audience data to MarTech platforms.
</p>
<figure>
  <img alt="" src="/images/4DataSystems/image5.jpg" style="" title="" />
  <figcaption>
    <i><strong>A top-level</strong></i
    ><i>&nbsp;view of audience syndication in Journal Development</i>
  </figcaption>
</figure>
<p class="c2">
  The database&nbsp;employs an off-the-shelf, cloud-based data warehouse product
  and set of automated data pipelines to process, collect, and store a variety
  of first- and third-party data.
</p>
<p class="c2">
  Once validated, logged and permission-checked, segmented audiences can be
  syndicated via a virtual computer to various AdTech platforms.
</p>
<p class="c2">
  The MIDB also offers analysts and colleagues a platform to do exploratory
  analysis on existing datasets, delivering fast computing and automated query
  scheduling for dataset readiness. This has become particularly useful for
  ongoing, customized&nbsp;reporting and large-scale augmentation of data.
</p>
<h3 class="c1" id="h.61nzanv1cfjp">
  What is our approach to compliance and governance?
</h3>
<p class="c2">
  Our approach to both data governance and data compliance is ever-evolving.
  However, there are some core and consistent principles that we adhere to.
</p>
<h4 class="c22 c21" id="h.ubtdhrf5mmi6">Processing</h4>
<ul class="c9 lst-kix_ci0kynjd2t4p-0 start">
  <li class="c2 c5 li-bullet-0">
    Our basis for processing data is primarily Legitimate Interest and as such
    our approach to data management is always focused on ensuring this basis is
    being met
  </li>
</ul>
<h4 class="c22 c21" id="h.qohgnlybwqt3">Governance</h4>
<ul class="c9 lst-kix_o7p10u2of8x4-0 start">
  <li class="c2 c5 li-bullet-0">
    Data security through quality assurance and adhering to administration by
    the principle of least privilege
  </li>
  <li class="c2 c5 li-bullet-0">Regular review and development of processes</li>
</ul>
<h4 class="c22 c21" id="h.bvkxn6obnaod">Compliance</h4>
<ul class="c9 lst-kix_l3pfka75tkbk-0 start">
  <li class="c2 c5 li-bullet-0">Compliance by design in system architecture</li>
  <li class="c2 c5 li-bullet-0">
    Regular review and development of requirements stipulated by various data
    laws
  </li>
  <li class="c2 c5 li-bullet-0">
    Systematically respond to all forms of requests from the Information
    Commissioner's Office
  </li>
</ul>
<h4 class="c22 c21" id="h.gmroe873hklw">Common sense</h4>
<ul class="c9 lst-kix_s7qfrya53zsc-0 start">
  <li class="c2 c5 li-bullet-0">
    Develop a culture of ‘data for good purposes’ among system engineers
  </li>
</ul>
<h3 class="c1" id="h.dgpabg1pvapu">What is our approach to reporting?</h3>
<p class="c2">
  Our top-level approach to reporting is to
  <strong
    >ensure all data are consolidated, cleaned, and presented in a table format
    within the MIDB</strong
  >. This is then connected to a front-end visualization platform to create
  <strong>an automated, single data source feed of insights</strong>&nbsp;for
  stakeholders.
</p>
<p class="c2">
  Reporting can cover anything in Journal Development: from the presentation of
  campaign results, right through to market analysis and opportunity
  exploration. As such, off-the-shelf data warehouse solutions from cloud
  providers are excellent for this purpose. Fast data processing allows for
  speedy dashboards and a comprehensive development environment for analysts.
</p>
<figure>
  <img alt="" src="/images/4DataSystems/image11.jpg" style="" title="" />
  <figcaption><i>Our approach to reporting</i></figcaption>
</figure>
<h4 class="c22 c21" id="h.wi3s4wvopt13">
  Attribution of Article Processing Charges (APCs) to marketing campaigns
</h4>
<p class="c2">
  <strong
    >Attributing APC value back to campaign spending is traditionally a
    difficult task.</strong
  >&nbsp;The peer review process can take many months to complete, meaning the
  value of conversions can remain unknown for a long time. On top of this, it is
  hard to ascertain a standard average order value, as APCs vary from journal to
  journal.
</p>
<p class="c2">
  We have leveraged the MIDB to combine multiple datasets and build a picture of
  what activity happens in the lead-up to all APC payments.
</p>
<p class="c2">
  The attribution method devised works by capturing data from each step in the
  process and working backward from the point of conversion to stitch
  interactions together. The process is as follows:
</p>
<ol class="c9 lst-kix_h2lufvboc5u5-0 start" start="1">
  <li class="c2 c5 li-bullet-0" data-li="1">
    Identify conversion within Phenom
  </li>
  <li class="c2 c5 li-bullet-0" data-li="2">
    Pair the owner in Phenom with the owner’s web analytics data
  </li>
  <li class="c2 c5 li-bullet-0" data-li="3">
    Search web analytics data for evidence of owner’s interactions with
    marketing campaigns
  </li>
  <li class="c2 c5 li-bullet-0" data-li="4">
    Attribute a proportional value of the manuscript in Phenom to the channels
    that contributed to it
  </li>
</ol>
<figure>
  <img alt="" src="/images/4DataSystems/image12.jpg" style="" title="" />
  <figcaption><i>Process for attribution</i></figcaption>
</figure>
<p class="c2">
  The data then outputs to a clean attribution table in the MIDB, updating daily
  as manuscript statuses progress through peer review.
</p>
<p class="c2">
  Although we continue to develop this process,
  <strong
    >the principle of working backward from the point of conversion will remain
    central</strong
  >&nbsp;going forward.
</p>
<h2 class="c36 c21" id="h.jg7jvekgeb3c">Marketing Operations</h2>
<p class="c15">
  The Marketing Operations team was formed in January 2022, bringing together
  the expertise of our communication platforms:
  <strong>email, paid, and social media</strong>. Designed to be a center of
  excellence,
  <strong
    >the team applies a thorough, expansive, and proactive approach to all
    marketing activities</strong
  >. This, in turn, improves processes, optimizes best practices for our
  ever-expanding programs, and drives greater engagement with our customers.
</p>
<p class="c15">&nbsp;Our primary functions are:</p>
<ul class="c9 lst-kix_3gvxee7dxu30-0 start">
  <li class="c4 li-bullet-0">
    managing and optimizing the marketing technology stacks and platforms
  </li>
  <li class="c4 li-bullet-0">automating data processes</li>
  <li class="c4 li-bullet-0">
    facilitating marketing campaigns in the most effective, efficient, and
    compliant manner
  </li>
  <li class="c4 li-bullet-0">
    advocating a data-driven approach to audience profiling and segmentation
  </li>
  <li class="c4 li-bullet-0">
    developing new capabilities to enhance engagement
  </li>
  <li class="c4 li-bullet-0">tracking and analyzing performance data</li>
</ul>
<h3 class="c15 c21" id="h.lwpeh8wjww4">Approach to campaigns</h3>
<figure>
  <img alt="" src="/images/4DataSystems/image10.jpg" style="" title="" />
  <figcaption>
    <i><strong>Campaign development journey</strong></i>
  </figcaption>
</figure>
<p class="c15">
  The campaign strategy — including its goals, messaging, and creativity — is
  defined by the individual marketing teams based on team, JDev, and wider
  business KPIs.
  <strong
    >We work closely with each team, offering a consultancy-like service to
    optimize campaign requirements and understand desired outcomes</strong
  >. We advise on:
</p>
<ul class="c9 lst-kix_mwzc4vqjq7t0-0 start">
  <li class="c4 li-bullet-0">
    journey framework and building — incorporating channels, touchpoints,
    communication frequency, engagement splitting, predictive analytics, and
    much more
  </li>
  <li class="c4 li-bullet-0">A/B test and learn tactics</li>
  <li class="c4 li-bullet-0">dynamic content and personalization</li>
  <li class="c4 li-bullet-0">reporting methods</li>
</ul>
<p class="c15">
  We encourage regular analysis of campaign performance with the idea to
  continually <strong>test &gt; learn &gt; re-iterate</strong>&nbsp;to gain a
  solid understanding of each customer segment and their behavioral trends.
  Promoting an insights-to-action approach enables us to constantly evolve,
  ensuring that communications remain relevant and we avoid oversaturating a
  particular customer segment.
</p>
<p class="c15">
  <strong
    >Post-campaign launch, automated reporting provides stakeholders with
    top-level performance metrics for tracking and evaluation</strong
  >. This data is used to draw insights that can inform subsequent marketing and
  engagement activity. With the support of our analysts,
  <strong
    >we aim to report beyond open and click rates and instead tie APC spend back
    to each campaign and the marketing channels used</strong
  >. We consider this to be the ultimate indicator of success for each campaign.
</p>
<h4 class="c15 c21" id="h.35sz2re8idop">Drive to automation</h4>
<p class="c15">
  <strong
    >In 2021, we developed and initiated a plan to transform our email marketing
    platform.</strong
  >&nbsp;The need to automate data processes and campaign execution was the core
  driver of this transformation. T<strong
    >he primary goal, aside from improving inefficiencies, was to remove all
    manual efforts marketers were previously undertaking.</strong
  >&nbsp;The plan focused on several areas for optimization:
</p>
<ul class="c9 lst-kix_ij9n1w6tqv8p-0 start">
  <li class="c4 li-bullet-0">data governance and hygiene</li>
  <li class="c4 li-bullet-0">data infrastructure and architecture</li>
  <li class="c4 li-bullet-0">data processing</li>
  <li class="c4 li-bullet-0">automation</li>
  <li class="c4 li-bullet-0">marketing execution and effectiveness</li>
  <li class="c4 li-bullet-0">process change</li>
  <li class="c4 li-bullet-0">stakeholder and user education</li>
  <li class="c4 li-bullet-0">application of in-built predictive analytics</li>
</ul>
<p class="c15">
  The most significant transformation came in 2022:
  <strong
    >the MIDB, an in-house built platform that centralizes data points from
    multiple internal data systems and platforms, was ingested into the email
    system and fed multiple layers of metadata.</strong
  >&nbsp;This completely revolutionized our data offering, and now provides us
  with a new and much-improved set of demographic, journal, manuscript, and
  customer-level information. This new infrastructure benefits us by:
</p>
<ul class="c9 lst-kix_hviibgduc4k3-0 start">
  <li class="c4 li-bullet-0">
    evolving our marketing approach across all communication platforms
  </li>
  <li class="c4 li-bullet-0">
    creating more sophisticated customer segments and profiles
  </li>
  <li class="c4 li-bullet-0">
    providing&nbsp;action- and behavioral-based segmentation
  </li>
  <li class="c4 li-bullet-0">offering more customer personalization</li>
  <li class="c4 li-bullet-0">providing tighter&nbsp;data governance</li>
  <li class="c4 li-bullet-0">
    synchronizing data across company platforms for greater consistency
  </li>
  <li class="c4 li-bullet-0">
    exchanging data across platforms for a multi-channel approach
  </li>
</ul>
<p class="c15">
  Along with the MIDB, we also have a Customer Relationship Management (CRM)
  system which is synchronized to the email marketing platform as an added data
  source.
</p>
<figure>
  <img alt="" src="/images/4DataSystems/image8.jpg" style="" title="" />
</figure>
<p class="c32"><i>Email System Data Flow</i></p>
<p class="c15 c11">
  <i><strong></strong></i>
</p>
<h4 class="c15 c21" id="h.oqp3k06crrz2">Paid Media activity</h4>
<p class="c15">
  Paid and social media platforms offer alternative marketing channels and are
  used for lead generation, raising brand awareness, journal promotion, and
  driving submissions. We proactively run tests across search engines and media
  advertising to monitor audience reach and engagement and determine which
  platforms have greater success at a campaign and individual level.
  Behavioral&nbsp;data from ad campaigns are passed on and tried on other ad
  channels as a method of remarketing. Similarly, tracking data is captured,
  ingested into the MIDB, and then made available in the email system.
</p>
<p class="c15 c11"><i></i></p>
<figure>
  <img alt="" src="/images/4DataSystems/image6.jpg" style="" title="" />
  <figcaption><i>Typical workflow for a Paid Media campaign</i></figcaption>
</figure>
<h4 class="c15 c21" id="h.t54pyus11m22">Campaign delivery</h4>
<p class="c15">
  So, we have optimized platforms, established a continually evolving process
  for our data sources, and now have a wealth of ever-expanding data points on
  offer for our marketers — how is this then leveraged to the optimum level?
</p>
<p class="c15">
  Where what were once isolated campaigns now fall into a wider customer
  journey, the concept of <strong>creating automated journeys</strong>&nbsp;is
  instead beginning to be at the forefront of marketers’ minds. Examples of this
  are welcome/onboarding and author submission journeys which provide a
  streamlined and timely set of relevant communications based on customer
  action. Not only does this improve the overall customer experience but it has
  also increased marketing teams’ capacity, enabling them to focus more time on
  campaign planning.
</p>
<p class="c15">
  Whilst automated journeys are continuously running at the required schedule,
  the majority of campaign delivery is in the form of one-off activities via the
  desired channel.
</p>
<p class="c15">
  The increased data points have also resulted in a greater opportunity for
  communications to be further optimized by including
  <strong>personalization and/or dynamic content</strong>&nbsp;at either contact
  or journal/manuscript level so a single template can serve multiple segments
  and be specific to their particular history.
</p>
<p class="c15">
  System-defined predictive analytics is added to all journeys which results in
  email messages being deployed at the time the individual is likely to
  interact.
</p>
<p class="c15">
  Here is where the multi-channel approach comes into play. Marketers are
  planning with a wider lens so all touchpoints relating to a single campaign
  are considered as part of a journey. In this instance, there are opportunities
  to segment audiences and assign them to multi-platforms before launch, for
  example, email disengagers to be served content via media channels. Likewise,
  this method can be applied mid-journey where customers who did/did not
  interact with email campaigns receive media touchpoints and <i>vice versa</i>.
</p>
<figure>
  <img alt="" src="/images/4DataSystems/image7.jpg" style="" title="" />
  <figcaption><i>Typical workflow for an email campaign</i></figcaption>
</figure>
<p class="c15 c11"><i></i></p>
<h2 class="c21 c36" id="h.ao2k37o6vdrq">Platforms managed by Data &amp; Ops</h2>
<p class="c2">
  The list below reflects the technology in use at the time of writing this
  documentation. As mentioned in previous sections, we intend to maintain
  flexibility over which platforms we use, reducing dependencies where possible.
</p>
<h5 class="c23 c21" id="h.igfihxtvlf5e">Advertising Platforms</h5>
<ul class="c9 lst-kix_ee566qdbseyl-0 start">
  <li class="c2 c5 li-bullet-0">Google Search Ads</li>
  <li class="c2 c5 li-bullet-0">Bing Search Ads</li>
  <li class="c2 c5 li-bullet-0">Twitter Ads</li>
  <li class="c2 c5 li-bullet-0">Facebook Ads</li>
</ul>
<h5 class="c21 c23" id="h.udjzfdtqq7on">Analytics Platforms</h5>
<ul class="c9 lst-kix_obio5fkvxisz-0 start">
  <li class="c2 c5 li-bullet-0">Google Analytics</li>
  <li class="c2 c5 li-bullet-0">Google Search Console</li>
</ul>
<h5 class="c23 c21" id="h.ak1t63e2zbbt">Email / CRM platforms</h5>
<ul class="c9 lst-kix_tjr6lzs5r5rh-0 start">
  <li class="c2 c5 li-bullet-0">Salesforce CRM</li>
  <li class="c2 c5 li-bullet-0">Salesforce Marketing Cloud</li>
  <li class="c2 c5 li-bullet-0">Dotmailer</li>
</ul>
<h5 class="c23 c21" id="h.rehk15pikepv">
  <br />Infrastructure / Data Technical
</h5>
<ul class="c9 lst-kix_w00x4rkft0yl-0 start">
  <li class="c2 c5 li-bullet-0">Asana Ticketing System</li>
  <li class="c2 c5 li-bullet-0">Team GitHub Repository</li>
  <li class="c2 c5 li-bullet-0">Team Confluence Library</li>
  <li class="c2 c5 li-bullet-0">Google Cloud Platform</li>
  <li class="c2 c5 li-bullet-0">Google BigQuery</li>
  <li class="c2 c5 li-bullet-0">Google Cloud Storage</li>
  <li class="c2 c5 li-bullet-0">Google Tag Manager</li>
  <li class="c2 c5 li-bullet-0">Funnel.io</li>
  <li class="c2 c5 li-bullet-0">Hotjar</li>
  <li class="c2 c5 li-bullet-0">GetSiteControl</li>
  <li class="c2 c5 li-bullet-0">Secure File Transfer Protocol (SFTP)</li>
</ul>
<h2 class="c36 c21" id="h.2gl9tbl031a7">Data &amp; Ops Dependencies</h2>
<p class="c2">
  We have two critical dependencies, one where we are the stakeholder and the
  other where we are the service provider.
</p>
<h3 class="c1" id="h.2thib96als4m">Dependency on Phenom Databases</h3>
<p class="c2">
  Our core data tables rely on regular updates from Phenom’s reporting
  databases. If for any reason those databases are unavailable or make a change
  that impacts the insight we are providing, we see the impact immediately. To
  mitigate against this, our systems are built to read automatically but retain
  the ability to manually refresh if needed. This guards against outages caused
  by database downtime during a scheduled refresh. The team also maintains
  strong working relationships with various data teams around the business, to
  ensure we have access to all available insight.
</p>
<h3 class="c1" id="h.f35ssotb3oam">
  Supplier dependency on wider Journal Development team
</h3>
<p class="c2">
  Data &amp; Ops supply data to the various engagement teams across Journal
  Development. We have an outfacing dependency on them as this data supports
  their campaign activity and drives their objectives. Any breakdown in
  supporting pipelines is detected within an hour due to round-the-clock
  monitoring. Issues are then worked on until fixed under high priority.
</p>
