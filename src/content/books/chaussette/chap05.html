---
order: 5
title: 'Researcher Engagement team'
menu: false
book: chaussette
layout: bookchapter.njk
---

<h2 class="c18" id="h.dc2qqhguiufn">
  <span class="c1">Why does researcher engagement matter? </span>
</h2>
<p class="c8">
  <span>Put simply, </span
  ><strong>researchers are at the heart of everything we do</strong>. Without
  their support and contributions, our journals would neither exist nor
  flourish. In Journal Development (JDev) we see the researcher as our core
  stakeholder and customer.
</p>
<h2 class="c18" id="h.3vw5awm9hk3z">Who are our researchers?</h2>
<p class="c8">
  We group researchers based on the four core roles they play in the publishing
  process: <strong>author, editor, reviewer, and reader</strong>. While it is
  unlikely that a researcher would fall into only one of these groups,
  ultimately, each has its own unique, individual needs and experience when
  interacting with us as a publisher.
</p>
<p class="c8">
  All of these researchers exist in mutually reinforcing, cyclical
  relationships. An extremely engaged editor is more likely to decide to handle
  a manuscript for a journal and select quality reviewers in an efficient
  manner. Equally, we expect engaged reviewers to work carefully, but quickly,
  to provide a high-quality report. This, in turn, helps satisfy the author’s
  expectations of receiving a rigorous, robust, and timely submission, peer
  review, and publication experience. In short, each researcher’s activity can
  improve — or, conversely, detract from — the experience of another. All of
  these roles also reinforce each other —&nbsp;for example, a researcher that
  has a good experience as a reviewer is more likely to value becoming an author
  (and vice versa).
</p>
<p class="c8">
  It is, therefore, the Researcher Engagement team’s responsibility to
  <strong>identify each researcher group’s wants and needs</strong>,
  <strong
    >maximize the efficiency of our current researcher engagement
    strategies</strong
  >, and
  <strong
    >devise new initiatives to drive engagement and improve the researcher
    experience</strong
  >&nbsp;at every touchpoint in the publishing process.
</p>
<figure>
  <img
    alt=""
    src="/images/5ResearcherEngagementTeam/image2.jpg"
    style=""
    title=""
  />
  <figcaption>
    <i>The relationship and interaction between researcher groups.</i>
  </figcaption>
</figure>
<h2 class="c18" id="h.26gj4fy9dy27">
  <strong>What is the Researcher Engagement team’s mission? </strong>
</h2>
<p class="c8">
  Our mission is to ensure that editors, reviewers, authors, and readers have
  the best experience possible when working or interacting with our journals.
</p>
<p class="c8">
  For more information about our stakeholders, please refer to
  <a
    class="c23"
    href="https://www.google.com/url?q=https://docs.google.com/document/u/2/d/1dgpSBLHEplbpWtp6LyIf8MnqEaweQ01IfAdf5oxSV7E/edit&amp;sa=D&amp;source=editors&amp;ust=1663570733317960&amp;usg=AOvVaw1GGmAW0xTxQkrpzVHHlh-S"
    >Ch.10</a
  >.
</p>
<h2 class="c18" id="h.4hd5ts2slue8">
  <strong
    >How does the Researcher Engagement team support our strategic
    pillars?</strong
  >
</h2>
<p class="c8">
  Dedicated to driving new and repeat submissions from all key researcher
  groups, the Researcher Engagement team plays a huge role in supporting our
  <strong>growth in content</strong>. In addition to the retention and expansion
  of our author base, the team’s activities also revolve around the conversion
  of editors and reviewers to submitting and, ideally, published authors. We
  also encourage editors to commission high-quality submissions from their peers
  and advocate the journal’s content within their communities.
</p>
<p class="c8">
  Our team-of-teams approach complements our company-wide commitment to being a
  <strong>publisher of publishers. </strong>The team’s unique structure demands
  collaboration both within and between different Researcher Engagement teams,
  enabling new initiatives to be implemented with greater speed, efficiency, and
  consideration.
</p>
<p class="c8">
  The Researcher Engagement team is the main point of contact for the Editorial
  and Publishing Operations teams and the Product teams, regarding insight into
  what constitutes <strong>operational excellence</strong>&nbsp;for each
  individual researcher group. The team contributes to the development of new
  internal features and processes that are designed to not only improve the
  overarching user experience on Phenom but speak to the specific requirements
  of each customer persona. Furthermore, our editorial and marketing activities
  strengthen the engagement of editors and reviewers to ensure that turnaround
  times remain competitive, and researchers commend us for our industry-leading
  peer review platform and user experience.
</p>
<p class="c8">
  The Researcher Engagement team’s commitment to enhancing the
  <strong>customer experience</strong>&nbsp;instills trust in the company’s
  ethos, practices, and services. We want researchers to have the best possible
  sentiment towards our journals, and one way we achieve this is through
  understanding, then resolving, our researchers’ pain points and tailoring the
  publication process to their needs in collaboration with other internal teams.
</p>
<p class="c8">
  We work closely with the Regional Development team to ensure our journals grow
  in strategic markets, and <strong>globally represent</strong>&nbsp;our
  research communities. We facilitate diversity in the geographical
  composition&nbsp;of our editorial boards, reviewer pool(s), and author base at
  both an individual journal and discipline level.
</p>
<p class="c8">
  The Researcher Engagement team seeks to ensure the
  <strong>widest distribution </strong>possible for all our content. We
  continuously select our high-quality research to showcase through different
  channels —&nbsp;driving usage and citations which, in turn, helps our journals
  reach indexing status in leading databases. The increased visibility and
  discoverability that indexing bestows upon a journal is critical in growing
  brand awareness, increasing submissions, and driving revenue.
</p>
<h2 class="c18" id="h.8iu4tc3dq51e">
  How does the Researcher Engagement team work?
</h2>
<h3 class="c11" id="h.llgha2i5azw">
  <strong>Who is </strong><strong>involved</strong>?
</h3>
<p class="c8">
  The Researcher Engagement team is a combination of Researcher Engagement
  Managers (REMs) and Marketing Managers (MMs), managed by Senior REMs and
  Senior MMs. The team is overseen by the Director of Journal Management and
  Director of Researcher Development. For each researcher group, there is at
  least one REM and one MM dedicated to investigating new methods — and
  optimizing existing methods — of editorial and marketing activity to ensure
  the group’s continued and increased engagement.
</p>
<figure>
  <img
    alt=""
    src="/images/5ResearcherEngagementTeam/image1.jpg"
    style=""
    title=""
  />
  <figcaption><i>Researcher Engagement team structure </i></figcaption>
</figure>
<p class="c8">​</p>
<h4 class="c9" id="h.cj47ll53lz7d">
  What do Researcher Engagement Managers (REMs) do?
</h4>
<p class="c8">The REM role is twofold, covering:</p>
<ol class="c10 lst-kix_256b245t0qtv-0 start" start="1">
  <li class="c6 li-bullet-0" data-li="1">
    managing the editorial engagement for a portfolio (of around 20-40 journals)
  </li>
  <li class="c6 li-bullet-0" data-li="2">
    monitoring and improving the engagement of a select researcher group
  </li>
</ol>
<p class="c8">
  Management of editorial engagement covers a range of tasks, including
  <strong>journal-level communications and policy development</strong>&nbsp;and
  relies heavily upon the REM’s skills in managing editor relationships. In
  conjunction with the MMs, REMs also handle editor recruitment, as well as the
  onboarding, training, and rotation of underperforming editors.
</p>
<p class="c8">
  More importantly, they act as
  <strong>the main point of contact for Senior Editors</strong
  >&nbsp;-&nbsp;nurturing and developing relationships with them to ensure they
  support the journal. Owing to this, within JDev, REMs can be considered the
  main point of contact for information on editorial matters, including Phenom
  processes and workflows.
</p>
<h4 class="c9" id="h.f91mtpq9aer6">What do Marketing Managers (MMs) do?</h4>
<p class="c17 c8 c25">
  MMs own the development and deployment of all marketing activity related to
  their target audience or researcher group. They work with a variety of
  internal stakeholders to understand the full journey each customer goes
  through when interacting with the publisher, and endeavor to engage with them
  at all touchpoints.
</p>
<p class="c17 c8 c25">
  The MM is responsible for creating the strategic vision for our customer
  segments, as well as achieving annual marketing objectives and KPIs through
  the implementation of integrated, multi-channel marketing campaigns across
  email, search, social, recommendation engines, and print. Each customer base
  can overlap, however, there is a tailored approach to each engagement
  strategy, depending on the researcher.
</p>
<p class="c17 c8 c25">
  The various MMs work closely in collaboration with one another. In the same
  way that editors, reviewers, authors, and readers are part of the same
  ecosystem, the Editor, Reviewer, Author, and Reader MMs all work within
  segments of the same customer environment and work towards common goals. These
  roles also have regional counterparts, who focus on engagement priorities
  across all researcher groups within strategic regions.
</p>
<h4 class="c9" id="h.ytgbv0j9jxk1">
  <strong>Does the perfect </strong><strong>union</strong
  ><strong>&nbsp;exist? The relationship between REMs and </strong
  ><strong>MMs</strong>
</h4>
<p class="c8">
  Elsewhere&nbsp;in scholarly publishing, it is rare to find marketing experts
  and journal managers working in the same teams to serve researchers.
</p>
<p class="c8">
  The Researcher Engagement team structure provides a way in which a publisher
  can connect with, and ensure the engagement of, their different researcher
  groups. Through the relationship between REMs and MMs (and the close
  connection to the wider JDev ecosystem) both groups are given greater scope to
  make sure their work has a significant impact.
</p>
<p class="c8">
  REMs benefit from enhanced insight and control over communications with
  researchers and the presentation of the journals, while MMs are given better
  access to and understanding of the customers they connect with. A project plan
  must have input from both groups.
</p>
<p class="c8">
  The magic of the Researcher Engagement team is a refusal to draw lines
  regarding ‘who does what.’ Instead, we maintain flexibility, which fosters
  creative and alternative ideas for improving the researcher experience.
</p>
<h4 class="c9" id="h.1h2k9686ff82">
  Our relationships outside of the Researcher Engagement team
</h4>
<p class="c8">
  We look to maximize all resources, from internal and external data to internal
  and external expertise. This is evident from our close working relationship
  with Publishing Insights, Data &amp; Operations, Marketing Insights, Editorial
  &amp; Publishing Operations, and our external Editorial Boards.
</p>
<p class="c8">
  These relationships provide insight into our authors, editors, reviewers, and
  readers through the analysis and understanding of market trends, audience
  data, and user behaviors. We monitor reports on the current manuscript
  handling stages and the capacity of our editors, the expected number of
  submissions from regions based on marketing activity and analyze the impact of
  regional trends on our projected growth.
</p>
<h2 class="c18" id="h.mbum3e9q7191">The teams within Researcher Engagement</h2>
<h3 class="c11" id="h.6nfinekwplii">The Editor Engagement team</h3>
<h4 class="c9" id="h.f6u2ena8fii9">Why is editor engagement important?</h4>
<p class="c8 c17">
  We are proud to work with international teams of editors that are experts in
  their respective fields of research. These leading academics steer the
  development of our journals and oversee the peer review process to ensure we
  publish useful research as efficiently as possible.
</p>
<p class="c17 c8">
  Our editors are the ultimate arbiters of quality control in our journals and
  are accountable for the research they publish. Our editorial models are
  reliant upon trusting them and their expertise, and without their active
  participation in the publishing process, our output halts.
</p>
<p class="c17 c8">
  Therefore, our editors must understand the requirements of their role
  —&nbsp;actively engaging with their responsibilities and championing their
  journals. On the other hand, it is equally important that we, as the
  publisher, recognize the contribution of our editors and make their roles as
  easy, enjoyable, and rewarding as possible.
</p>
<h4 class="c9" id="h.e5xejs37za4t">
  What are the Editor Engagement team’s objectives?
</h4>
<ul class="c10 lst-kix_53h36d6u7djy-0 start">
  <li class="c6 li-bullet-0">
    Build and manage relationships with all journal editors and ensure editors
    are aligned with the team’s wider journal development strategies
  </li>
  <li class="c6 li-bullet-0">
    Manage the editor recruitment process, and ensure all journals have the
    required number of active and engaged editors to handle submission volumes
  </li>
  <li class="c6 li-bullet-0">
    Ensure editors are active, engaged, and satisfied with their role, and have
    a comprehensive understanding of required expectations
  </li>
  <li class="c6 li-bullet-0">
    Nurture editors into authors and incentivize editor submissions to our
    journal portfolio
  </li>
</ul>
<h4 class="c9" id="h.y9yqdatqg5g3">
  <strong>What are the Editor Engagement team’s key </strong
  ><strong>responsibilities</strong>?
</h4>
<ul class="c10 lst-kix_77lsjyekk0eb-0 start">
  <li class="c6 li-bullet-0">Editor relationship management</li>
  <ul class="c10 lst-kix_77lsjyekk0eb-1 start nest1">
    <li class="c4 li-bullet-0">
      <strong>Build and maintain relationships</strong>&nbsp;with editors across
      the portfolio of journals
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_77lsjyekk0eb-0">
  <li class="c6 li-bullet-0">Editor recruitment</li>
  <ul class="c10 lst-kix_77lsjyekk0eb-1 start nest1">
    <li class="c4 li-bullet-0">
      <strong>Recruitment</strong>&nbsp;of Chief Editors and Senior Editors
    </li>
    <li class="c4 li-bullet-0">
      Develop Academic Editor
      <strong>recruitment strategy and processes</strong>
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_77lsjyekk0eb-0">
  <li class="c6 li-bullet-0">Editor onboarding and lifecycle</li>
  <ul class="c10 lst-kix_77lsjyekk0eb-1 start nest1">
    <li class="c4 li-bullet-0">
      Develop and manage the
      <strong>automated editor onboarding process </strong>
    </li>
    <li class="c4 li-bullet-0">
      Manage and expand the <strong>Editor Community platform</strong>
    </li>
    <li class="c4 li-bullet-0">
      <strong>Managing communications</strong>&nbsp;and campaigns to editors
    </li>
    <li class="c4 li-bullet-0">
      Create and deploy&nbsp;the <strong>editor survey</strong>&nbsp;to collect
      valuable feedback and refine current processes or implement new
      initiatives
    </li>
    <li class="c4 li-bullet-0">
      Working with&nbsp;editors to
      <strong>drive the growth of our journals</strong>
    </li>
    <li class="c4 li-bullet-0">
      <strong>Maintain editor engagement </strong>using editor-level data
      regarding turnaround times, declinations, rejections, and more &nbsp;
    </li>
    <li class="c4 li-bullet-0">
      Review the performance of our editors and
      <strong>rotate disengaged editors</strong>
    </li>
    <li class="c4 li-bullet-0">
      Educate editors on the
      <strong>Special Issue proposal assessment process</strong>
    </li>
    <li class="c4 li-bullet-0">
      Gain <strong>insights on editor needs and behavior</strong>, and support
      in the creation of a company-wide editor customer persona
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_77lsjyekk0eb-0">
  <li class="c6 li-bullet-0">Editor recognition</li>
  <ul class="c10 lst-kix_77lsjyekk0eb-1 start nest1">
    <li class="c4 li-bullet-0">
      Identify
      <strong>new methods of recognizing editors’ contributions</strong>&nbsp;to
      our journals (e.g., digital badges and certificates, editor hall of fame,
      early career researcher hub)
    </li>
    <li class="c4 li-bullet-0">
      <strong>Showcase and promote editor publications</strong>&nbsp;and
      increase reach and visibility of high-quality content
    </li>
  </ul>
</ul>
<h4 class="c9" id="h.t53dzabgw8dg">
  How is engagement measured in the Editor Engagement team?
</h4>
<p class="c8">
  The Editor Engagement team defines an engaged editor as one that:
</p>
<ul class="c10 lst-kix_6b8shhni9i48-0 start">
  <li class="c6 li-bullet-0">
    regularly accepts to handle assigned manuscripts
  </li>
  <li class="c6 li-bullet-0">
    responds to manuscript invitations in a timely manner
  </li>
  <li class="c6 li-bullet-0">
    interacts with email campaigns and communications
  </li>
  <li class="c6 li-bullet-0">publishes their own manuscripts in the journal</li>
  <li class="c6 li-bullet-0">
    advocates the journal within their respective research community
  </li>
</ul>
<h3 class="c11" id="h.90xo0q8m1qx9">The Reviewer Engagement Team</h3>
<h4 class="c9" id="h.bp5dhgyaxvdb">Why is reviewer engagement important?</h4>
<p class="c8">
  In traditional publishing spheres, the role of the reviewer has often been
  overlooked. However, we believe that a reviewer’s journey with a publisher
  must be valued, and made exceptional, at all stages. This not only leads to
  the publisher’s success but is also an important way to show appreciation for
  reviewers’ invaluable hard work.
</p>
<p class="c8">
  Reviewers’ experiences decide whether they review for a journal again, or even
  whether they consider submitting their own research. A well-conducted
  invitation process and encouragement to submit their report in a timely manner
  will assure reviewers that should they submit a manuscript to the journal, it
  will also progress efficiently throughout the peer review process.
</p>
<p class="c8">
  Equally, the knowledge that a high-quality report is expected of a reviewer
  tells them that we strive for all manuscripts to be&nbsp;of the highest
  standard. Better reviews lead to better research.
</p>
<h4 class="c9" id="h.vwmybstgmx0u">
  What are the Reviewer Engagement team’s objectives?
</h4>
<ul class="c10 lst-kix_unpbj08u8fmb-0 start">
  <li class="c6 li-bullet-0">
    Develop ways to capture potential reviewer data and provide editors with
    high-quality reviewers
  </li>
  <li class="c6 li-bullet-0">
    Engage with potential reviewers to work with our journals
  </li>
  <li class="c6 li-bullet-0">
    Educate reviewers on how to provide the best possible review through
    training and resources
  </li>
  <li class="c6 li-bullet-0">
    Review the performance of our reviewers and develop new ways to track this
  </li>
  <li class="c6 li-bullet-0">
    Reduce manuscript turn-around times through quick acceptance times for
    invitations and timely submission of reports
  </li>
  <li class="c6 li-bullet-0">
    Moderate the experience of our reviewers and develop new ways to improve
    this
  </li>
  <li class="c6 li-bullet-0">
    Recognize the contributions of reviewers through recognition programs
  </li>
  <li class="c6 li-bullet-0">
    Encourage reviewers to accept future invitations to review
  </li>
  <li class="c6 li-bullet-0">
    Promote opportunities to convert into authors or editors
  </li>
  <li class="c6 li-bullet-0">
    Closely work with Product on how best to improve systems for the reviewer
    experience
  </li>
</ul>
<h4 class="c9" id="h.z73bcwf8uxl">
  What are the Reviewer Engagement team’s key responsibilities?
</h4>
<ul class="c10 lst-kix_vhv4lliqxl6y-0 start">
  <li class="c6 li-bullet-0">Reviewer recruitment</li>
  <ul class="c10 lst-kix_vhv4lliqxl6y-1 start nest1">
    <li class="c4 li-bullet-0">
      Encourage existing authors, previous reviewers, and unsuccessful editor
      applicants to consider reviewing or re-reviewing and
      <strong>joining the Reviewer Pool</strong>
    </li>
    <li class="c4 li-bullet-0">
      Investigate <strong>new avenues</strong>&nbsp;<strong
        >in which potential reviewers may be discovered</strong
      >
    </li>
    <li class="c4 li-bullet-0">
      <strong>Ensure reviewer diversity </strong>by targeting regions and
      subject areas
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_vhv4lliqxl6y-0">
  <li class="c6 li-bullet-0">Reviewer training</li>
  <ul class="c10 lst-kix_vhv4lliqxl6y-1 start nest1">
    <li class="c4 li-bullet-0">
      Develop <strong>reviewer resources</strong>&nbsp;to improve the overall
      quality of all reviewer reports
    </li>
    <li class="c4 li-bullet-0">
      <strong>Provide open resources</strong>&nbsp;to assist in first-time
      reviewing for early career researchers
    </li>
    <li class="c4 li-bullet-0">
      Create a <strong>community hub</strong>&nbsp;where reviewers can find
      support, resources, and training as well as enhance a community feel and
      recognition
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_g0sjurgbeb3z-0 start">
  <li class="c6 li-bullet-0">Management of the reviewer lifecycle</li>
  <ul class="c10 lst-kix_g0sjurgbeb3z-1 start nest1">
    <li class="c4 li-bullet-0">
      Develop a company-wide
      <strong>reviewer customer persona</strong>&nbsp;that can be used when
      managing/enhancing/creating all reviewer communications
    </li>
    <li class="c4 li-bullet-0">
      Improve the
      <strong>overall experience of the reviewer journey </strong>within Phenom
      based on drop-off points, frustrations, and survey responses
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_g0sjurgbeb3z-0">
  <li class="c6 li-bullet-0">Reviewer recognition</li>
  <ul class="c10 lst-kix_g0sjurgbeb3z-1 start nest1">
    <li class="c4 li-bullet-0">
      Provide <strong>incentives for reviewers</strong>&nbsp;such as APC
      discounts
    </li>
    <li class="c4 li-bullet-0">
      Develop
      <strong>programs and tools to support reviewer recognition</strong>, such
      as certificates and badges and the Reviewer Hall of Fame
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_e9l1rrvsdspc-0 start">
  <li class="c6 li-bullet-0">Brand awareness</li>
  <ul class="c10 lst-kix_e9l1rrvsdspc-1 start nest1">
    <li class="c4 li-bullet-0">
      Enhance the awareness as a<strong>&nbsp;great place for reviewers </strong
      >through the involvement of industry-wide campaigns such as Peer Review
      Week
    </li>
    <li class="c4 li-bullet-0">
      <strong>Launch webinars</strong>&nbsp;promoting new publishing initiatives
      to global attendees increasing recruitment, training opportunities, and
      overall brand recognition
    </li>
  </ul>
</ul>
<h4 class="c9" id="h.nlgnyu2e1pwq">
  <strong>How is engagement measured in the Reviewer Engagement team?</strong>
</h4>
<p class="c8">Our reviewers are considered to be engaged if they:</p>
<ul class="c10 lst-kix_r4q6894xbqy-0 start">
  <li class="c6 li-bullet-0">quickly respond&nbsp;to invitations to review</li>
  <li class="c6 li-bullet-0">review every round of a paper</li>
  <li class="c6 li-bullet-0">submit high-quality reports</li>
  <li class="c6 li-bullet-0">submit promptly</li>
  <li class="c6 li-bullet-0">return to the journal to review again</li>
  <li class="c6 li-bullet-0">encourage others to review for our journals</li>
  <li class="c6 li-bullet-0">are interested in training opportunities</li>
  <li class="c6 li-bullet-0">submit as an author</li>
  <li class="c6 li-bullet-0">apply to join an editorial board</li>
</ul>
<h3 class="c11" id="h.jch64g51u2ap">The Author Engagement team</h3>
<h4 class="c9" id="h.f6o4atqkgq6a">Why is author engagement important?</h4>
<p class="c8">
  The core objective of the author engagement team is to go beyond driving
  submissions.
</p>
<p class="c8">
  We realize the importance of engaging with authors throughout the publishing
  experience and beyond — the publication of a paper is only one important
  milestone in the research journey, but it is not the last step. We support
  authors in promoting and communicating their work to maximize its impact not
  only within the research community but also within wider society.
</p>
<p class="c8">
  Authors are at the center of our business model, as our revenue is driven by
  the collection of article processing charges (APCs) from accepted articles.
  Therefore, our journals must continue to appeal to authors as reputable venues
  for their all-important research.
</p>
<h4 class="c9" id="h.na2t2hw8g8n7">
  What are the Author Engagement team’s objectives?
</h4>
<ul class="c10 lst-kix_6qdc7gwmnzoi-0 start">
  <li class="c6 li-bullet-0">
    Identify and engage with prospective authors from strategic markets on both
    an individual journal and discipline level
  </li>
  <li class="c6 li-bullet-0">
    Nurture existing authors and facilitate repeat authorship
  </li>
  <li class="c6 li-bullet-0">
    Recognize authors’ high-quality contributions to our portfolio of journals
  </li>
  <li class="c6 li-bullet-0">
    Improve authors’ experience when submitting to, and publishing in, our
    journals
  </li>
</ul>
<h4 class="c9" id="h.7pz4avfhwng7">
  What are the Author Engagement team’s key responsibilities?
</h4>
<ul class="c10 lst-kix_3e7mbxr0mfyk-0 start">
  <li class="c6 li-bullet-0">Author submissions</li>
  <ul class="c10 lst-kix_3e7mbxr0mfyk-1 start nest1">
    <li class="c4 li-bullet-0">
      Encourage submissions from strategic markets by
      <strong>identifying prospective authors</strong>&nbsp;based on journal and
      discipline
    </li>
    <li class="c4 li-bullet-0">
      <strong>Monitor submission and author retention rate</strong>
    </li>
    <li class="c4 li-bullet-0">
      Identify <strong>new campaigns </strong>to drive submissions from both
      prospective and existing authors
    </li>
    <li class="c4 li-bullet-0">
      <strong>Working with wider JDev teams</strong>,
      including<strong>&nbsp;</strong>the Partnership and Content Development
      teams to
      <strong>drive submissions to partner journals and Special Issues</strong>
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_3e7mbxr0mfyk-0">
  <li class="c6 li-bullet-0">Author care</li>
  <ul class="c10 lst-kix_3e7mbxr0mfyk-1 start nest1">
    <li class="c4 li-bullet-0">
      Build and maintain<strong>&nbsp;an automated email journey </strong>to
      nurture published authors for retainment as stakeholders
    </li>
    <li class="c4 li-bullet-0">
      <strong>Educate and keep authors informed</strong>&nbsp;about
      pre-submission and post-publication author services, best practices, new
      processes or policies, share important journal updates, and prompt them to
      share and promote their research with their network
    </li>
    <li class="c4 li-bullet-0">
      <strong>Re-engage with lapsed authors</strong>&nbsp;to submit future
      manuscripts to our journals
    </li>
    <li class="c4 li-bullet-0">
      Oversee the <strong>author survey</strong>&nbsp;to collect valuable
      feedback and refine current processes or implement new initiatives to
      improve the author's experience
    </li>
    <li class="c4 li-bullet-0">
      Gain <strong>insight into author needs</strong>&nbsp;and behavior, to
      support the creation of a company-wide author customer persona
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_3e7mbxr0mfyk-0">
  <li class="c6 li-bullet-0">Author recognition</li>
  <ul class="c10 lst-kix_3e7mbxr0mfyk-1 start nest1">
    <li class="c4 li-bullet-0">
      <strong>Showcase published content </strong>to both existing and potential
      authors, and increase the reach and visibility of high-quality content, in
      collaboration with the Reader Engagement team
    </li>
    <li class="c4 li-bullet-0">
      <strong>Developing streams of initiatives that recognize authors</strong>’
      contributions to our journals (e.g., Article of the Year Award, article
      highlights, impact snapshot), and identify new, high-impact methods of
      recognition
    </li>
  </ul>
</ul>
<h4 class="c9" id="h.wkhi2nl0z7tu">
  How do the Author Engagement team measure engagement?
</h4>
<p class="c8">The Author Engagement team considers an engaged author to:</p>
<ul class="c10 lst-kix_n949tdwsweo1-0 start">
  <li class="c6 li-bullet-0">
    complete the submission process for their manuscript
  </li>
  <li class="c6 li-bullet-0">promote their research on social media</li>
  <li class="c6 li-bullet-0">
    interact with email campaigns and communications
  </li>
  <li class="c6 li-bullet-0">re-submit to our journal portfolio</li>
  <li class="c6 li-bullet-0">
    demonstrate an interest in contributing as an editor and/or reviewer with us
  </li>
  <li class="c6 li-bullet-0">
    recommend/advocate for our journals to other researchers
  </li>
</ul>
<h3 class="c11" id="h.ouhi7tx7z1m1">The Reader Engagement Team</h3>
<h4 class="c9" id="h.3ab5w3isujrx">Why is reader engagement important?</h4>
<p class="c8">
  Reader engagement is pivotal to the scientific publishing ecosystem. An author
  will publish their article with the aim of their research being read, shared,
  and cited. The reader of their article will be looking for research to support
  their own studies and interests, as well as looking for potential
  collaborations with others in their field.
</p>
<p class="c8">
  Often the first time a researcher engages with a journal is when they read an
  article from it. They may not even know which journal they are reading at the
  time&nbsp;(due to the disaggregated way researchers now find articles). Often
  though, readers will remember a relevant article they read when looking to
  cite research in their own work — this is the point at which a read evolves
  into a citation and a researcher actively becomes aware of a journal.
</p>
<p class="c8">
  Reading is still the basis of our industry — we help research to be published
  so it can be read and used. Most publishers do not consider or acknowledge
  readers in the way we do in JDev, but to us, they are a core researcher group
  in our drive to provide value to all of the roles researchers play in the
  publishing process.
</p>
<h4 class="c9" id="h.r93ielvojr6n">
  <strong>What are the Reader Engagement t</strong>eam’s objectives?
</h4>
<ul class="c10 lst-kix_ay6ty57hvv24-0 start">
  <li class="c6 li-bullet-0">
    Ensure readers return to our journals to find relevant content
  </li>
  <li class="c6 li-bullet-0">
    Convert readers into authors, editors, and reviewers
  </li>
  <li class="c6 li-bullet-0">
    Develop global marketing strategies to improve the overall readership,
    website engagement, and citations of articles
  </li>
  <li class="c6 li-bullet-0">
    Encourage sharing and increase dissemination of journal articles
  </li>
  <li class="c6 li-bullet-0">
    Increase citations of journal articles (especially based on predetermined
    priority groupings)
  </li>
  <li class="c6 li-bullet-0">Tracking and driving increased page views</li>
  <li class="c6 li-bullet-0">
    Increase abstracting and indexing coverage of our journals
  </li>
</ul>
<h4 class="c9" id="h.yrgzk99ovz3u">
  What are the Reader Engagement team's key responsibilities?
</h4>
<ul class="c10 lst-kix_mjwx8dj8sslu-0 start">
  <li class="c6 li-bullet-0">Drive usage and citations</li>
  <ul class="c10 lst-kix_mjwx8dj8sslu-1 start nest1">
    <li class="c4 li-bullet-0">
      Deploying
      <strong>campaigns for dissemination and engagement</strong>&nbsp;with
      content
    </li>
    <li class="c4 li-bullet-0">
      Determine key areas of focus for a
      <strong>citation strategy</strong>&nbsp;that will incorporate impact,
      discipline, journal, and (where relevant) regional considerations
    </li>
    <li class="c4 li-bullet-0">
      Developing the <strong>readership content strategy</strong>&nbsp;of our
      portfolio of journals to ensure maximum engagement and visibility of
      published articles
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_mjwx8dj8sslu-0">
  <li class="c6 li-bullet-0">Brand awareness</li>
  <ul class="c10 lst-kix_mjwx8dj8sslu-1 start nest1">
    <li class="c4 c22 li-bullet-0">
      Lead <strong>public engagement strategy</strong>&nbsp;in collaboration
      with the Content and Brand Marketing Manager
    </li>
    <li class="c4 li-bullet-0">
      Increasing overall <strong>brand awareness</strong>&nbsp;of the journals
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_mjwx8dj8sslu-0">
  <li class="c6 li-bullet-0">Targeting and reporting</li>
  <ul class="c10 lst-kix_mjwx8dj8sslu-1 start nest1">
    <li class="c4 li-bullet-0">
      Determining <strong>priority countries, regions, and </strong
      ><strong>institutions</strong>&nbsp;
    </li>
    <li class="c4 li-bullet-0">
      Ensuring effective<strong>&nbsp;targeting and reporting</strong>&nbsp;are
      in place using channel and website metrics such as Google Analytics
    </li>
  </ul>
</ul>
<ul class="c10 lst-kix_mjwx8dj8sslu-0">
  <li class="c6 li-bullet-0">Supporting conversion</li>
  <ul class="c10 lst-kix_mjwx8dj8sslu-1 start nest1">
    <li class="c4 li-bullet-0">
      Developing an understanding of how
      <strong>readers convert into authors, editors, and reviewers </strong>
    </li>
    <li class="c4 li-bullet-0">
      Working with the relevant audience Marketing Managers to improve the
      <strong>reader journey</strong>&nbsp;
    </li>
  </ul>
</ul>
<h4 class="c9" id="h.e7zmbhbsh0q1">
  How is engagement measured in the Reader Engagement team?
</h4>
<p class="c8">
  The Reader Engagement team considers an engaged reader as someone who:
</p>
<ul class="c10 lst-kix_l7yfe82dtcr1-0 start">
  <li class="c6 li-bullet-0">also acts as an editor, reviewer, or author</li>
  <li class="c6 li-bullet-0">cites our articles in their own work</li>
  <li class="c6 li-bullet-0">returns to our web pages frequently</li>
  <li class="c6 li-bullet-0">
    takes an active role in the dissemination of articles published in our
    journals
  </li>
  <li class="c6 li-bullet-0">
    shares articles on social media and community platforms
  </li>
  <li class="c6 li-bullet-0">follows us on social media</li>
  <li class="c6 li-bullet-0">advocates our journals</li>
</ul>
