---
order: 2
title: 'From Open Access to Open Science: fueling our success in the service of global knowledge'
menu: false
book: chaussette
layout: bookchapter.njk
---

<h2 class="c11 c3" id="h.hzujyaep4rg8">
  <strong>Why is Open Access the </strong><strong>first step</strong>&nbsp;to
  Open Science?
</h2>
<p class="c1">
  <strong
    >A key element of our mission as a publisher is to maximize the impact of
    scientific research through openness and global collaboration </strong
  >and everyone in the company truly believes in our motto: science works best
  when research is open!
</p>
<p class="c1">
  Open Access (OA) has long been at the core of our approach, and our innovative
  workflows and platforms have resulted in us being one of the largest fully OA
  publishers in the world.
</p>
<p class="c1">
  We are a leader in the OA market because we are unencumbered by multiple
  systems, workflows, and bespoke editorial practices. Our
  <strong>radical openness</strong
  ><strong
    >, scalability, and willingness to keep innovating allow us to take a
    leadership position</strong
  >&nbsp;in the development of a new type of publishing experience that
  explicitly creates sustainable services for Open Science (OS).
</p>
<p class="c1">
  We have the platform (Phenom) abilities, and resources to take an
  evidence-informed approach to open science solutions and interventions —
  piloting at a small scale before testing and rolling them out across the fully
  OA portfolio, at ever-increasing scales.
</p>
<p class="c1">
  We primarily exist as a company to drive and facilitate open access
  publishing, however, the purpose of creating an open science system is to
  innovate and drive change at scale in a way that meets the rapidly changing
  requirements of science communication. Succeeding in this will ensure we
  continue to evolve into a publisher of the future.
</p>
<p class="c1 quote">
  ‘Open Science — fueling our success in the service of global knowledge’
</p>
<p class="c1">The three key aims of our Open Science approach are:</p>
<ol class="c8 lst-kix_6dbk75dg5egw-0 start" start="1">
  <li class="c1 c19 li-bullet-0" data-li="1">
    Identify options to advance Open Access and commercialize the benefits of
    Open Science
  </li>
</ol>
<ol class="c8 lst-kix_vg8cngtqlngf-0 start" start="2">
  <li class="c1 c19 li-bullet-0" data-li="2">
    Coordinate the implementation of Open Science solutions across the
    organization
  </li>
</ol>
<ol class="c8 lst-kix_wkkt7q8l7won-0 start" start="3">
  <li class="c1 c19 li-bullet-0" data-li="3">
    Reinforce our Open Science credentials and leadership
  </li>
</ol>
<h2 class="c3 c11" id="h.ljbuasr0isfd">
  <strong>What is Open </strong><strong>Access</strong>?
</h2>
<p class="c1">
  Open Access (OA) means research is free to read, reuse, and share. In
  traditional publishing models, readers pay through subscriptions — usually via
  university libraries. In our OA business model, authors pay at the point of
  acceptance of their article with an APC (Article Processing Charge).
</p>
<p class="c1">
  Core to our mission is the belief that science works best when research is
  open. Open Access enables greater visibility, sharing, and potential for
  real-world impact of research. Having its roots in the
  <a
    class="c4"
    href="https://www.google.com/url?q=https://www.budapestopenaccessinitiative.org/read/&amp;sa=D&amp;source=editors&amp;ust=1663570714257190&amp;usg=AOvVaw2eO4Jbt1lhQpU_kBqtCAxs"
    >Budapest Open Access Initiative</a
  >, open access is quickly becoming the norm for science publishing, and it is
  increasingly the expectation that funded research will be published OA.
</p>
<p class="c1">
  All our journal articles are published under the Creative Commons Attribution
  License (CC-BY). This license means that they can be downloaded, shared, and
  reused without restriction, as long as the original authors are properly
  cited.
</p>
<h3 class="c29 c3" id="h.6vdwnm7xx8gx">Why Open Access?</h3>
<p class="c1">
  Publishing OA under a CC-BY license not only accelerates scientific
  discoveries but also encourages collaborations and ensures that the research
  is compliant with the OA mandates from funders and institutes.
</p>
<p class="c1">
  OA publishing increases the potential benefit of research globally. Important
  research published in subscription access journals may not be available to
  researchers in the same field depending on their institution. This leads to
  inequity in publishing, disadvantages researchers in poorer countries
  (particularly in the Global South), and hinders the progress of science. While
  not completely without problems of equity, Open Access means that, at the very
  least, articles are freely available to a global audience.
</p>
<h2 class="c11 c3" id="h.km29vnqrorrt">The progression to Open Science</h2>
<h3 class="c3 c29" id="h.xbj3oglpzty8">What is Open Science?</h3>
<p class="c1">
  One of the clearest ways to understand Open Science is through the perspective
  of a researcher. In 2017, Jeff Rouder, Professor of Cognitive Sciences​ at the
  School of Social Sciences​, University of California, Irvine, tweeted:​
</p>
<p class="c1">​</p>
<p class="c36 quote">
  What is Open Science? It is endeavoring to preserve the rights of others to
  reach independent conclusions about your data and work​
</p>
<p class="c1">
  At the heart of this statement is the ability for others to evaluate a
  researcher’s work in full, including the data or code underlying the findings
  of the paper, and the methods. It also means sharing the limitations of
  experiments or those that do not work out as expected or give negative
  results.
</p>
<p class="c1">
  The evaluation of a researcher’s work should not only focus on the final
  publication output but all other output types that the research produces —
  including data and software. and the expertise and time researchers invest in
  peer review — as either an author, editor, or reviewer. All of this
  contributes to the advancement of science and allows others to build on its
  findings. If a researcher upholds standards of integrity, then all these
  contributions are valid and the scholarship should be acknowledged.
</p>
<p class="c1">
  Open Science has now been embraced as a global concept and practice,
  exemplified through the formal ratification in 2022 of the
  <a
    class="c4"
    href="https://www.google.com/url?q=https://en.unesco.org/science-sustainable-future/open-science/recommendation&amp;sa=D&amp;source=editors&amp;ust=1663570714259256&amp;usg=AOvVaw0-hEmSTUUpEhaZuV2hyWdC"
    >UNESCO Recommendation on Open Science</a
  >&nbsp;by more than 190 countries worldwide.
</p>
<p class="c1">The UNESCO report states that the aims of Open Science are</p>
<ul class="c8 lst-kix_l1qm36u0tad-0 start">
  <li class="c1 c19 li-bullet-0">
    to make multilingual scientific knowledge openly available, accessible, and
    reusable for everyone
  </li>
  <li class="c1 c19 li-bullet-0">
    to increase scientific collaborations and sharing of information for the
    benefit of science and society
  </li>
  <li class="c1 c19 li-bullet-0">
    to open the processes of scientific knowledge creation, evaluation, and
    communication to societal actors beyond the traditional scientific community
    ​
  </li>
</ul>
<a id="t.6c6c36a84098dd3922d219629bf570fe56198622"></a><a id="t.0"></a>
<table class="c38 callbox">
  <tbody>
    <tr class="c34">
      <td class="c10" colspan="1" rowspan="1">
        <h4 class="c3 c20 callbox" id="h.8hnci6pbaphs">
          The UNESCO Recommendations on Open Science
        </h4>
        <p class="c30 callbox">
          The General Conference of the United Nations Educational, Scientific
          and Cultural Organization (UNESCO), held in Paris from November 9 to
          24, 2021, recognized the need of addressing complex and interconnected
          environmental, social and economic complex and interconnected
          challenges (such as climate change, biodiversity loss, poverty,
          technology, and innovation gaps).
        </p>
        <p class="c30 callbox">
          UNESCO responded to these challenges by considering the effectiveness
          of more open, transparent, collaborative, inclusive,
          interdisciplinary, and reliable knowledge to inform society and
          support an evidence-based decision-making process.
        </p>
        <p class="c37 callbox">
          This Recommendation aims to provide a framework for open science
          policy and practice and to outline a common definition, shared values,
          principles, and standards for Open Science at the international level.
          The core values of Open Science stem from the rights-based, ethical,
          epistemological, economic, legal, political, social,
          multi-stakeholder, and technological implications of opening science
          to society and broadening the principles of openness to the whole
          cycle of scientific research.
        </p>
      </td>
    </tr>
  </tbody>
</table>
<p class="c1">
  The recommendations not only define Open Science but also provide a lens
  through which scholarly publishers can drive innovation in the communication
  of scholarly knowledge. They provide a framework within which a business can
  align the value of shareholders with those of their stakeholders.
</p>
<figure>
  <img alt="" src="/images/2OpenScience/image3.jpg" style="" title="" />
  <figcaption>
    <i
      ><strong
        >Values and principles of Open Science (image adapted from the UNESCO
        Recommendations 2021)</strong
      ></i
    >
  </figcaption>
</figure>
<h2 class="c11 c3" id="h.pwiansslo5yw">
  Translating Open Science into our mission and business objectives
</h2>
<p class="c1">
  Taking the UNESCO values and principles as the basis of our approach to Open
  Science, we can translate the company’s core strategic pillars into relevant
  Open Science goals within each pillar, namely:
</p>
<p class="c1">
  <strong>Growth in Content​: </strong>we aim to&nbsp;ensure all relevant and
  sound scientific knowledge that <i>can</i>&nbsp;be published
  <i>is</i>&nbsp;published and meets community standards of transparency, trust,
  integrity, and, consequently, reusability​.
</p>
<p class="c1">
  <strong>Publisher of Publishers​: </strong>we provide discrete, cost-effective
  solutions for publishers that meet the needs of researchers practicing open
  science and funders and institutions who mandate that publishers report on
  these practices.​
</p>
<p class="c1 c9"><strong></strong></p>
<p class="c1">
  <strong>Operational​ Excellence​: </strong>we&nbsp;produce cost-effective,
  scalable, and efficient solutions that minimize complexity, optimize
  transparency and interoperability, and enable articles to be evaluated within
  the wider context of other research outputs and practices, such as data
  sharing, and preprinting.​
</p>
<p class="c1">
  <strong>Customer Experience​: </strong>we help build&nbsp;trust in our
  services by adopting a collaborative, evidence-informed approach, and
  drastically reducing the burden on researchers and publishers when adopting
  open science practices, such as data sharing, preprinting, and reporting.
</p>
<p class="c1">
  <strong>Globally Represent​: </strong>we build equity for both producers and
  consumers of scientific knowledge by making our publications open to all,
  regardless of geography, language, gender, generations, and resources, and by
  being sensitive to regional and Indigenous communities and cultures.​
</p>
<p class="c1">
  <strong>Widest Distribution</strong>: we fuel the&nbsp;integration and
  adoption of open scholarly persistent identifiers (PIDs) and the creation of
  enriched metadata so that the articles we publish, and all the associated
  research objects, are discoverable by all.​
</p>
<h3 class="c29 c3" id="h.7h4cwhhnn1ma">A service provider for Open Science</h3>
<p class="c1">
  The shift to open access has changed the key stakeholders of publishing from
  readers and librarians to authors, funders, and institutions, as is discussed
  further in the chapter ‘Generating value for our customers and company.’
</p>
<p class="c1">
  This shift in the core customer base has also changed the incentive from
  exclusivity, where journals want to publish a few high-impact articles, to a
  more inclusive landscape where the volume of articles published is linked to
  revenue. While publishing volume is a key consideration, however, we also have
  a responsibility to
  <strong>uphold high standards of research integrity and ethics</strong>.
</p>
<figure>
  <img alt="" src="/images/2OpenScience/image1.jpg" style="" title="" />
  <figcaption>
    <i>The changing focus for publishers of open science</i>
  </figcaption>
</figure>
<p class="c1">
  The move to open science requires publishers to shift focus from only
  peer-reviewed articles to a range of research outputs — as well as serving an
  expanding customer base that creates or funds the content (or wants to be able
  to access, mine, and reuse it).
</p>
<h4 class="c1 c3" id="h.y3zvfvik50to">Research practices</h4>
<h4 class="c1 c3" id="h.3wj1fu5gzc2g">
  <strong
    >While conducting peer review and publishing articles will always remain
    core to reputable publishing, we also need to ensure that the article is
    embedded and </strong
  ><strong
    >connected to a much wider range of research objects that help validate the
    findings of the article and make it trustworthy</strong
  >.
</h4>
<h4 class="c1 c35 c3" id="h.86sx7innkefj"></h4>
<p class="c1 c3" id="h.xaymnm87wh21">
These include relevant datasets and code but also protocols and registered
reports where appropriate, as well as preprints and peer review reports
themselves. Such transparency will help to increase the quality of articles.
It also helps to mitigate against fraudulent practices and improves trust and
confidence in research.
</p>
<h4 class="c1 c3" id="h.pvmhn5i26p31">&nbsp;</h4>
<p>
Our Open Science approach, therefore, aims to support a range of different
research practices in our journals, such as preprinting, data sharing, and
transparent peer review. We also aim to create a great user experience for
authors, so that they can share their articles and data much faster, earlier,
and easier.
</p>
<h4 class="c3 c26" id="h.u9sdxwb4ueaf">Discoverability</h4>
<p>
<strong
  >Discoverability has become a key feature of open science. To meet the needs
  of customers who reuse and mine scholarly research, we aim to publish
  content that is as FAIR as possible, by making our content Findable,
  Accessible, Interoperable, and Reusable. Such</strong
><strong>&nbsp;</strong>discoverability is enabled by an open and
machine-readable infrastructure that not only links the different research
outputs to each other, but also to the researchers, institutions, and funders
(whose help is essential in creating and funding their production).
</p>
<h4 class="c1 c3 c35" id="h.cmerodnhg4dx"></h4>
<p>
<strong
  >We have a reputation for high production standards that help others
  discover the work of researchers. This includes high-quality, open metadata
  and the use of PIDs such as </strong
><strong
  ><a
    class="c4"
    href="https://www.google.com/url?q=https://info.orcid.org/what-is-orcid/&amp;sa=D&amp;source=editors&amp;ust=1663570714266526&amp;usg=AOvVaw0eD_eCahqIflsNxz9HBJR4"
    >ORCID</a
  ></strong
><strong>&nbsp;and </strong
><strong
  ><a
    class="c4"
    href="https://www.google.com/url?q=https://ror.org/&amp;sa=D&amp;source=editors&amp;ust=1663570714266824&amp;usg=AOvVaw38-aX6cLZNn4gikCBd3TU8"
    >ROR</a
  ></strong
><strong>&nbsp;in our workflows</strong><strong>.</strong
><strong
  >&nbsp;The use of PIDs is essential not only to connect research outputs,
  increasing their discoverability but also to grant interoperability among
  different stakeholders and systems.</strong
>
</p>
<h3 class="c29 c3" id="h.s5j7bsn9ji3z"><strong>Cultural change </strong></h3>
<figure>
  <img alt="" src="/images/2OpenScience/image2.jpg" style="" title="" />
  <figcaption>
    <i><strong>The drivers of Open Science</strong></i>
  </figcaption>
</figure>
<p class="c1">
  The culture of Open Science is crucial to driving a sustainable and innovative
  ecosystem of scholarly communication. As an advocate for Open Science, we are
  actively involved in a wide range of OS initiatives to shape the values,
  behaviors, and practices of the research community as well as the scholarly
  publishing industry.
</p>
<p class="c1">We are:</p>
<ol class="c8 lst-kix_47orgoxfztva-0 start" start="1">
  <li class="c1 c19 li-bullet-0" data-li="1">
    A founding organization for the relaunch of
    <a
      class="c4"
      href="https://www.google.com/url?q=https://sfdora.org/&amp;sa=D&amp;source=editors&amp;ust=1663570714268290&amp;usg=AOvVaw2bSxNRn2ZNZGCQ5YnPRekL"
      >The Declaration on Research Assessment (DORA)</a
    >&nbsp;in 2017 — an initiative finalized to improve how the output of
    scientific research is evaluated by funding agencies, academic institutions,
    and other parties
  </li>
  <li class="c1 c19 li-bullet-0" data-li="2">
    A member of the
    <a
      class="c4"
      href="https://www.google.com/url?q=https://openscience.eu/open-science-policy-platform-final-report&amp;sa=D&amp;source=editors&amp;ust=1663570714268692&amp;usg=AOvVaw2AOOTk_NNGXyU-aB3m95pI"
      >EC Open Science Policy Platform</a
    >&nbsp;2016-2020 — an advisory group that sets a list of recommendations for
    all the relevant actors and tracks the status of implementation of open
    science practices
  </li>
  <li class="c1 c19 li-bullet-0" data-li="3">
    Supporting Open Infrastructure — a collaboration between scholarly
    publishers, researchers, and other interested parties to promote the
    unrestricted availability of scholarly citation data and abstracts, and to
    improve the discovery, navigation, retrieval, and access to research
    resources through the use of open infrastructure and PIDs
  </li>
  <ol class="c8 lst-kix_47orgoxfztva-1 start nest1" start="1">
    <li class="c1 c5 li-bullet-0" data-li="1">
      <a
        class="c4"
        href="https://www.google.com/url?q=https://i4oc.org/&amp;sa=D&amp;source=editors&amp;ust=1663570714269083&amp;usg=AOvVaw05D_yZIAOV55OiinjRu8ZZ"
        >I4OC</a
      >&nbsp;— founding org
    </li>
    <li class="c1 c5 li-bullet-0" data-li="2">
      <a
        class="c4"
        href="https://www.google.com/url?q=https://i4oa.org/&amp;sa=D&amp;source=editors&amp;ust=1663570714269352&amp;usg=AOvVaw3oqJ_5_bhdllW6Cb-arBwW"
        >I4OA</a
      >&nbsp;— founding org
    </li>
    <li class="c1 c5 li-bullet-0" data-li="3">
      <a
        class="c4"
        href="https://www.google.com/url?q=https://www.project-freya.eu/Plone/en&amp;sa=D&amp;source=editors&amp;ust=1663570714269646&amp;usg=AOvVaw3e3EgUO7xjsrLbJRtahRoQ"
        >FREYA</a
      >&nbsp;— project partner
    </li>
  </ol>
</ol>
<ol class="c8 lst-kix_47orgoxfztva-0" start="4">
  <li class="c1 c19 li-bullet-0" data-li="4">
    Supporting
    <a
      class="c4"
      href="https://www.google.com/url?q=https://asapbio.org/publishyourreviews&amp;sa=D&amp;source=editors&amp;ust=1663570714269996&amp;usg=AOvVaw3_Ev_hgYEncncmYTuFoH0H"
      >ASAPbio Publish Your Reviews</a
    >&nbsp;— an initiative led by ASAPbio to encourage peer reviewers to publish
    their reviews alongside the preprint of an article
  </li>
</ol>
<p class="c1">
  By helping to drive the culture changes around open science, we are confident
  that we will fulfill our mission to be the leading provider of Open Access
  publishing services and advance the global expansion of Open Science.
</p>
<div></div>
