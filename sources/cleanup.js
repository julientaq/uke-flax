function semanticHTML() {
  // removeEmptySpan() //firefox only for some reasons?
  semInline()
  semPre()
  cleanPre()
  // cleanCode();
  cleanImg()
  cleanTable()
  cleanList()
  removeAllEmpty()
  // setsrc();
  semEndnotes()
  // ChapTitle()
  changeImgUrl()
  recreateFigss()
  reorderTitle()
  deleteEmpty()
  finalizeFile()
  prepareForEleventy()
}

function prepareForEleventy() {
  //remove meta
  document.querySelector('h1, .title').remove();
}

function reorderTitle() {
  document.querySelectorAll('h1,h2,h3,h4,h5,h6').forEach(title =>{
    switch (title.tagName) {
      case 'h1':
        title.insertAdjacentHTML('beforebegin', `<h2>${title.innerHTML}</h2>`)
        break;

      case 'h2':
        title.insertAdjacentHTML('beforebegin', `<h3>${title.innerHTML}</h3>`)
        break;
      case 'h3':
        title.insertAdjacentHTML('beforebegin', `<h4>${title.innerHTML}</h4>`)
        break;
      case 'h4':
        title.insertAdjacentHTML('beforebegin', `<h5>${title.innerHTML}</h5>`)
        break;
      case 'h5':
        title.insertAdjacentHTML('beforebegin', `<h6>${title.innerHTML}</h6>`)
        break;
      default:
        break;
    }
  }) 
    
  
}



function recreateFigss() {  document
  //recreate figures
    .querySelectorAll('p, h1, h2,h3,h4,h5,h6, blockquote, pre')
    .forEach((possibleParent) => {
      if (possibleParent.parentElement == document.body) {
        possibleParent.classList.add('possibleParent')
      }
    })

  document.querySelectorAll('img').forEach((img) => {
    const figure = document.createElement('figure')
    figure.innerHTML = img.outerHTML
    let figcaption = document.createElement('figcaption')
    if (img.closest('.possibleParent')?.nextElementSibling.tagName == 'H6') {
      figcaption.innerHTML =
        figcaption.innerHTML +
        img.closest('.possibleParent').nextElementSibling.innerHTML
      img.closest('.possibleParent').nextElementSibling.remove()
    }
    if (
      img.closest('.possibleParent')?.previousElementSibling.tagName == 'H6'
    ) {
      figcaption.innerHTML =
        figcaption.innerHTML +
        img.closest('.possibleParent').previousElementSibling.innerHTML
      img.closest('.possibleParent').previousElementSibling.remove()
    }
    figure.insertAdjacentElement('beforeend', figcaption)
    img.closest('.possibleParent')?.insertAdjacentElement('afterend', figure)
    img.closest('.possibleParent')?.remove()
  })
  document.querySelectorAll('.possibleParent').forEach(el => { el.classList.remove('possibleParent') })
}

function deleteEmpty() {
  document
    .querySelectorAll(
      'p, div, h1,h2,h3,h4,h5,h6,span, blockquote, cite, pre, code'
    )
    .forEach((thing) => {
      if (thing.textContent.length == 0) {
        thing.remove()
      }
    })
}

function changeImgUrl() {
  document.querySelectorAll('img').forEach((img) => {
    imgURL = window.location.pathname
      .split('/')
      [window.location.pathname.split('/').length - 1].replace('Ch.', '')
      .replace('.html', '')

    img.src =
      '/images/' +
      imgURL +
      '/' +
      img.src.split('/')[img.src.split('/').length - 1]
  })
}

function semInline() {
  var items = document.querySelectorAll('span')
  var bold = 0
  var italic = 0
  var highlight = 0
  var quote = 0
  var callbox = 0
  var inlineCode = 0
  var preCode = 0

  for (let i = 0; i < items.length; i++) {
    var item = items[i]
    var elHTML = item.innerHTML
    var itemFontWeight = window
      .getComputedStyle(item, null)
      .getPropertyValue('font-weight')
    var itemFontStyle = window
      .getComputedStyle(item, null)
      .getPropertyValue('font-style')
    var itemFontColor = window
      .getComputedStyle(item, null)
      .getPropertyValue('color')
    var itemBkgColor = window
      .getComputedStyle(item, null)
      .getPropertyValue('background-color')
    var itemFont = window
      .getComputedStyle(item, null)
      .getPropertyValue('font-family')
    var valX = [204, 183, 217, 247]
    var prefix = ''
    var suffix = ''

    if (itemFontWeight == 700) {
      prefix = '<strong>' + prefix
      suffix = suffix + '</strong>'
      bold++
    }

    if (itemFontStyle == 'italic') {
      prefix = '<i>' + prefix
      suffix = suffix + '</i>'
      italic++
    }

    if (itemFontColor == 'rgb(255, 0, 0)') {
      prefix = '<em>' + prefix
      suffix = suffix + '</em>'
      highlight++
    }

    // for (let j = 0; j < valX.length; j++) {
    //     if (itemBkgColor == 'rgb(' + valX[j] + ', ' + valX[j] + ', ' + valX[j] + ')') {
    //         prefix = '<code>' + prefix;
    //         suffix = suffix + '</code>';
    //         inlineCode++;
    //     }
    // }

    if (itemFont == '"Courier New"') {
      prefix = '<code>' + prefix
      suffix = suffix + '</code>'
      inlineCode++
    }

    if (itemBkgColor == 'rgb(0, 255, 255)') {
      item.closest('p').classList.add('quote')
      quote++
    }

    if (bold > 0 || italic > 0 || highlight > 0) {
      if (itemFont == '"Space Mono"') {
        prefix = '<pre><code>' + prefix
        suffix = suffix + '</code></pre>'
        preCode++
      }
    }

    if (itemFont == '"Ubuntu"') {
      item.parentNode.classList.add('callbox')
      if (item.closest('table')) {
        item.closest('table').classList.add('callbox')
      }

      callbox++
    }

    if (bold > 0 || italic > 0 || highlight > 0 || inlineCode > 0) {
      item.insertAdjacentHTML('beforebegin', prefix + elHTML + suffix)
      item.parentNode.removeChild(item)
    }
  }
  console.log(bold + ' Bold')
  console.log(italic + ' Italic')
  console.log(highlight + ' highlight')
  console.log(quote + ' quote')
  console.log(callbox + ' callbox')
  console.log(inlineCode + ' inlineCode')
}

function semPre() {
  var items = document.querySelectorAll('pre')
  var blocks = 0

  items.forEach((pre) => {
    if (
      pre.previousElementSibling &&
      pre.previousElementSibling.tagName === 'PRE'
    ) {
      pre.innerHTML = pre.previousElementSibling.innerHTML + pre.innerHTML
      pre.previousElementSibling.parentNode.removeChild(
        pre.previousElementSibling
      )
      blocks++
    }
  })

  console.log(blocks + ' Pre')
}

function cleanPre() {
  document.querySelectorAll('pre').forEach((pre) => {
    pre.innerHTML = pre.innerHTML + '&#10;'
    const removed = pre.parentElement
    if (removed.tagName == 'LI') {
      console.log('li !')
      // removed.insertAdjacentElement("beforeend", pre);
    } else {
      removed.insertAdjacentElement('beforebegin', pre)
      removed.remove()
    }
  })
  semPre()
}

// function cleanCode() {
//     var items = document.querySelectorAll('code');
//     items.forEach(code =>{
//         if (code.previousElementSibling && code.previousElementSibling.tagName === "CODE") {
//             code.innerHTML = code.previousElementSibling.innerHTML + code.innerHTML;
//             code.previousElementSibling.parentNode.removeChild(code.previousElementSibling);
//         };
//     });
// }

function cleanImg() {
  var figcaption = 0
  document.querySelectorAll('img').forEach((img) => {
    const removed = img.parentElement
    removed.insertAdjacentElement('beforebegin', img)
    removed.remove()
    img.style = ''
    var newFigure = document.createElement('figure')
    newFigure.innerHTML = img.outerHTML

    if (img.closest('h6')) {
      var imgH6Parent = img.closest('h6')
      imgH6Parent.insertAdjacentElement('beforebegin', img)
      imgH6Parent.remove()
    }

    if (img.closest('p')) {
      var imgParent = img.closest('p')
      imgParent.insertAdjacentElement('beforebegin', img)
      imgParent.remove()
    }

    if (img.nextElementSibling && img.nextElementSibling.tagName === 'H6') {
      var newFigcaption = document.createElement('figcaption')
      newFigcaption.innerHTML = img.nextElementSibling.innerHTML
      img.nextElementSibling.parentNode.removeChild(img.nextElementSibling)
      newFigure.innerHTML = newFigure.innerHTML + newFigcaption.outerHTML
      figcaption++
    }

    img.insertAdjacentElement('beforebegin', newFigure)
    img.remove()
  })

  console.log(figcaption + ' figcaption')
}

function cleanTable() {
  var thead = 0
  document.querySelectorAll('td').forEach((td) => {
    var tdBkg = window
      .getComputedStyle(td, null)
      .getPropertyValue('background-color')
    // console.log(tdBkg);
    var valX = [204, 183, 217, 247]
    for (let j = 0; j < valX.length; j++) {
      if (tdBkg == 'rgb(' + valX[j] + ', ' + valX[j] + ', ' + valX[j] + ')') {
        td.classList.add('thead')
        thead++
      }
    }
  })

  document.querySelectorAll('table').forEach((table) => {
    if (table.nextElementSibling && table.nextElementSibling.tagName === 'H6') {
      var newFigcaption = document.createElement('figcaption')
      newFigcaption.innerHTML = table.nextElementSibling.innerHTML
      table.nextElementSibling.parentNode.removeChild(table.nextElementSibling)
      table.insertAdjacentElement('afterend', newFigcaption)
    }
  })

  console.log(thead + ' thead')
}

function cleanList() {
  var cleaned = 0
  document.querySelectorAll('ul').forEach((ul) => {
    var liIndent = window
      .getComputedStyle(ul.firstElementChild, null)
      .getPropertyValue('margin-left')
    var valX = ['144px', '96px']
    for (let j = 0; j < valX.length; j++) {
      if (liIndent == valX[j]) {
        ul.classList.add('nest' + j)
        ul.previousElementSibling.innerHTML =
          ul.previousElementSibling.innerHTML + ul.outerHTML
        ul.parentNode.removeChild(ul)
        cleaned++
      }
    }
  })

  document.querySelectorAll('ol').forEach((ol) => {
    var liIndent = window
      .getComputedStyle(ol.firstElementChild, null)
      .getPropertyValue('margin-left')
    var valX = ['144px', '96px']
    for (let j = 0; j < valX.length; j++) {
      if (liIndent == valX[j]) {
        ol.classList.add('nest' + j)
        ol.previousElementSibling.innerHTML =
          ol.previousElementSibling.innerHTML + ol.outerHTML
        ol.parentNode.removeChild(ol)
        cleaned++
      }
    }
  })

  document.querySelectorAll('ol').forEach((ol) => {
    var start = ol.getAttribute('start')
    ol.querySelectorAll('li').forEach((li) => {
      li.setAttribute('data-li', start)
      start++
    })
  })

  console.log(cleaned + ' ul in ul and ol in ol')
}

function removeAllEmpty() {
  document.querySelectorAll('p,li').forEach((el) => {
    if (el.innerHTML == '' || el.innerHTML == '&nbsp;') {
      el.remove()
    }
  })
}

function removeEmptySpan() {
  document.querySelectorAll('span').forEach((sp) => {
    if (!sp.getAttribute('style') && !sp.getAttribute('class')) {
      console.log('sp', sp)
      if (sp.outerHTML) {
        
      sp.outerHTML = sp.outerHTML.replace('<span>', '')
      sp.outerHTML = sp.outerHTML.replace('</span>', '')
      }
    }
  })
}

// function setsrc() {
//     chap = document.querySelector('section').getAttribute('id');
//
//     document.querySelectorAll('img').forEach(img => {
//         src = img.getAttribute('src');
//         img.setAttribute('src', src.replace("images", "img/" + chap));
//         console.log(img.getAttribute('src'));
//     });
// }

function semEndnotes() {
  document.querySelectorAll('section hr:last-of-type').forEach((hr) => {
    var newH2 = document.createElement('h2')
    newH2.innerHTML = 'Notes'
    hr.insertAdjacentElement('afterend', newH2)
    hr.classList.add('notes')
  })

  document.querySelectorAll('div').forEach((div) => {
    if (div.querySelector('a[href^="#ftnt_"]') !== null) {
      div.classList.add('note-item')
      console.log('note-item')
    }
  })

  document.querySelectorAll('a[href^="#ftnt"]').forEach((ftnt) => {
    ftnt.innerHTML = ftnt.innerHTML.replace('[', '')
    ftnt.innerHTML = ftnt.innerHTML.replace(']', '')
  })

  document.querySelectorAll('a[href^="#cmnt"]').forEach((comment) => {
    comment.parentElement.classList.add('comment')
    console.log('comment')
  })

  console.log('notes')
}

function ChapTitle() {
  document.querySelectorAll('p.title').forEach((i) => {
    var newDiv = document.createElement('div')
    var newH1 = document.createElement('H1')
    var content = i.innerText
    console.log(content)

    i.insertAdjacentElement('afterend', newDiv)
    newDiv.insertAdjacentElement('afterbegin', newH1)
    newDiv.classList.add('section')
    newH1.insertAdjacentText('afterbegin', content)
    newH1.classList.add('sectiontitle')
    i.remove()

    console.log('ChapTitle')
  })
}

function finalizeFile() {
  let yaml = `---
order: 
title: '${document.querySelector('h1, .title').innerHTML}' 
menu: false
book: 
layout: bookchapter.njk
---

`

  console.log(yaml + document.body.innerHTML)
}

semanticHTML()
